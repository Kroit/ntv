// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine.h"
#include "Engine/GameViewportClient.h"
//#include "DisplayClusterViewportClient.h"
#include "CanvasTypes.h"
#include "CarrotViewportClient.generated.h"

/**
 * 
 */
UCLASS()
class CARROT_API UCarrotViewportClient : public UGameViewportClient
{
	GENERATED_BODY()

public:
	UCarrotViewportClient(FVTableHelper& Helper);
	virtual ~UCarrotViewportClient();

	virtual void Init(struct FWorldContext& WorldContext, UGameInstance* OwningGameInstance, bool bCreateNewAudioDevice = true) override;
	virtual void Draw(FViewport* Viewport, FCanvas* SceneCanvas) override;

private:
	FTexture2DRHIRef viewportTex2d;

public:
	FTexture2DRHIRef GetRenderTargetTex2D();
};

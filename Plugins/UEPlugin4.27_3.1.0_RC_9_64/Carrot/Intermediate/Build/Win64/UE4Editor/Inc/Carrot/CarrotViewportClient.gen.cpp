// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Carrot/Public/CarrotViewportClient.h"
#include "Engine/Classes/Engine/Engine.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCarrotViewportClient() {}
// Cross Module References
	CARROT_API UClass* Z_Construct_UClass_UCarrotViewportClient_NoRegister();
	CARROT_API UClass* Z_Construct_UClass_UCarrotViewportClient();
	ENGINE_API UClass* Z_Construct_UClass_UGameViewportClient();
	UPackage* Z_Construct_UPackage__Script_Carrot();
// End Cross Module References
	void UCarrotViewportClient::StaticRegisterNativesUCarrotViewportClient()
	{
	}
	UClass* Z_Construct_UClass_UCarrotViewportClient_NoRegister()
	{
		return UCarrotViewportClient::StaticClass();
	}
	struct Z_Construct_UClass_UCarrotViewportClient_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCarrotViewportClient_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UGameViewportClient,
		(UObject* (*)())Z_Construct_UPackage__Script_Carrot,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCarrotViewportClient_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "CarrotViewportClient.h" },
		{ "ModuleRelativePath", "Public/CarrotViewportClient.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCarrotViewportClient_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCarrotViewportClient>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCarrotViewportClient_Statics::ClassParams = {
		&UCarrotViewportClient::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000ACu,
		METADATA_PARAMS(Z_Construct_UClass_UCarrotViewportClient_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCarrotViewportClient_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCarrotViewportClient()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCarrotViewportClient_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCarrotViewportClient, 2268931538);
	template<> CARROT_API UClass* StaticClass<UCarrotViewportClient>()
	{
		return UCarrotViewportClient::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCarrotViewportClient(Z_Construct_UClass_UCarrotViewportClient, &UCarrotViewportClient::StaticClass, TEXT("/Script/Carrot"), TEXT("UCarrotViewportClient"), false, nullptr, nullptr, nullptr);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif

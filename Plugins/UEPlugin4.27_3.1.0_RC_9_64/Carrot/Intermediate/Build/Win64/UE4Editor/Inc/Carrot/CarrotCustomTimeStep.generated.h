// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CARROT_CarrotCustomTimeStep_generated_h
#error "CarrotCustomTimeStep.generated.h already included, missing '#pragma once' in CarrotCustomTimeStep.h"
#endif
#define CARROT_CarrotCustomTimeStep_generated_h

#define HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotCustomTimeStep_h_25_SPARSE_DATA
#define HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotCustomTimeStep_h_25_RPC_WRAPPERS
#define HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotCustomTimeStep_h_25_RPC_WRAPPERS_NO_PURE_DECLS
#define HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotCustomTimeStep_h_25_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCarrotCustomTimeStep(); \
	friend struct Z_Construct_UClass_UCarrotCustomTimeStep_Statics; \
public: \
	DECLARE_CLASS(UCarrotCustomTimeStep, UFixedFrameRateCustomTimeStep, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Carrot"), NO_API) \
	DECLARE_SERIALIZER(UCarrotCustomTimeStep)


#define HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotCustomTimeStep_h_25_INCLASS \
private: \
	static void StaticRegisterNativesUCarrotCustomTimeStep(); \
	friend struct Z_Construct_UClass_UCarrotCustomTimeStep_Statics; \
public: \
	DECLARE_CLASS(UCarrotCustomTimeStep, UFixedFrameRateCustomTimeStep, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Carrot"), NO_API) \
	DECLARE_SERIALIZER(UCarrotCustomTimeStep)


#define HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotCustomTimeStep_h_25_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCarrotCustomTimeStep(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCarrotCustomTimeStep) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCarrotCustomTimeStep); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCarrotCustomTimeStep); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCarrotCustomTimeStep(UCarrotCustomTimeStep&&); \
	NO_API UCarrotCustomTimeStep(const UCarrotCustomTimeStep&); \
public:


#define HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotCustomTimeStep_h_25_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCarrotCustomTimeStep(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCarrotCustomTimeStep(UCarrotCustomTimeStep&&); \
	NO_API UCarrotCustomTimeStep(const UCarrotCustomTimeStep&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCarrotCustomTimeStep); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCarrotCustomTimeStep); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCarrotCustomTimeStep)


#define HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotCustomTimeStep_h_25_PRIVATE_PROPERTY_OFFSET
#define HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotCustomTimeStep_h_22_PROLOG
#define HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotCustomTimeStep_h_25_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotCustomTimeStep_h_25_PRIVATE_PROPERTY_OFFSET \
	HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotCustomTimeStep_h_25_SPARSE_DATA \
	HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotCustomTimeStep_h_25_RPC_WRAPPERS \
	HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotCustomTimeStep_h_25_INCLASS \
	HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotCustomTimeStep_h_25_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotCustomTimeStep_h_25_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotCustomTimeStep_h_25_PRIVATE_PROPERTY_OFFSET \
	HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotCustomTimeStep_h_25_SPARSE_DATA \
	HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotCustomTimeStep_h_25_RPC_WRAPPERS_NO_PURE_DECLS \
	HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotCustomTimeStep_h_25_INCLASS_NO_PURE_DECLS \
	HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotCustomTimeStep_h_25_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class CarrotCustomTimeStep."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CARROT_API UClass* StaticClass<class UCarrotCustomTimeStep>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotCustomTimeStep_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS

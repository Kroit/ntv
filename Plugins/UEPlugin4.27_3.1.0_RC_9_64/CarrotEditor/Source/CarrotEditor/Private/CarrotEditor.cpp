// Copyright Epic Games, Inc. All Rights Reserved.

#include "CarrotEditor.h"
#include "CarrotEditorStyle.h"
#include "CarrotEditorCommands.h"
#include "Misc/MessageDialog.h"
#include "ToolMenus.h"

#include "LevelEditor.h"
#include "Engine/LevelScriptBlueprint.h"
#include "Engine/TextureRenderTarget2D.h"
#include "EdGraph/EdGraph.h"
#include "K2Node_CallFunction.h"
#include "K2Node_AddDelegate.h"
#include "K2Node_CustomEvent.h"
#include "K2Node_SwitchString.h"
#include "K2Node_MacroInstance.h"
#include "Core.h"
#include "K2Node_Event.h"
#include "Editor\UATHelper\Public\IUATHelperModule.h"

DEFINE_LOG_CATEGORY(LogCarrotEditor);

static const FName CarrotEditorTabName("CarrotEditor");

#define LOCTEXT_NAMESPACE "FCarrotEditorModule"

void FCarrotEditorModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module

	FCarrotEditorStyle::Initialize();
	FCarrotEditorStyle::ReloadTextures();

	FCarrotEditorCommands::Register();

	PluginCommands = MakeShareable(new FUICommandList);

	PluginCommands->MapAction(
		FCarrotEditorCommands::Get().PluginAction,
		FExecuteAction::CreateRaw(this, &FCarrotEditorModule::PluginButtonClicked),
		FCanExecuteAction());

	UToolMenus::RegisterStartupCallback(FSimpleMulticastDelegate::FDelegate::CreateRaw(this, &FCarrotEditorModule::RegisterMenus));

	fCarrotEditorModule = this;

	InitDLL();
}

void FCarrotEditorModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.

	if (hModule != NULL)
	{
		::FreeLibrary(hModule);
	}

	UToolMenus::UnRegisterStartupCallback(this);

	UToolMenus::UnregisterOwner(this);

	FCarrotEditorStyle::Shutdown();

	FCarrotEditorCommands::Unregister();
}

void FCarrotEditorModule::PluginButtonClicked()
{
	if (GEditor != NULL)
	{
		if (GEditor->GetWorldContexts().Num() > 0)
		{
			UWorld* World = GEditor->GetWorldContexts()[0].World();

			FString projName = FApp::GetProjectName();
			UE_LOG(LogCarrotEditor, Warning, TEXT("Project Name: %s"), *projName);

			if (World->GetLevels().Num() > 0)
			{
				int carrotLvl = FindCarrotLevel(World);

				if (carrotLvl > -1)
				{
					FString levelName = World->GetFName().ToString();
					UE_LOG(LogCarrotEditor, Warning, TEXT("Level Name: %s"), *levelName);

					hasTrackingData = false;
					receivers.Reset();
					senders.Reset();
					commands.Reset();

					ULevelScriptBlueprint* scriptBlue = World->GetLevels()[carrotLvl]->GetLevelScriptBlueprint();

					TArray<UEdGraph*> UbergraphPages = scriptBlue->UbergraphPages;

					int graphCnt = UbergraphPages.Num();

					for (int g = 0; g < graphCnt; g++)
					{
						UEdGraph* graph = UbergraphPages[g];

						GetComponentsFromGraph(graph);
					}

					BuildStructure(projName, levelName);
				}
			}
		}
	}
}

void FCarrotEditorModule::RegisterMenus()
{
	// Owner will be used for cleanup in call to UToolMenus::UnregisterOwner
	FToolMenuOwnerScoped OwnerScoped(this);

	{
		UToolMenu* Menu = UToolMenus::Get()->ExtendMenu("LevelEditor.MainMenu.Window");
		{
			FToolMenuSection& Section = Menu->FindOrAddSection("WindowLayout");
			Section.AddMenuEntryWithCommandList(FCarrotEditorCommands::Get().PluginAction, PluginCommands);
		}
	}

	{
		UToolMenu* ToolbarMenu = UToolMenus::Get()->ExtendMenu("LevelEditor.LevelEditorToolBar");
		{
			FToolMenuSection& Section = ToolbarMenu->FindOrAddSection("Settings");
			{
				FToolMenuEntry& Entry = Section.AddEntry(FToolMenuEntry::InitToolBarButton(FCarrotEditorCommands::Get().PluginAction));
				Entry.SetCommandList(PluginCommands);
			}
		}
	}
}

void FCarrotEditorModule::InitDLL()
{
	std::wstring regSubKey = L"SOFTWARE\\Carrot\\Engine";

	std::wstring regValue(L"CarrotPath");
	std::wstring valueFromRegistry;

	try
	{
		valueFromRegistry = GetStringValueFromHKLM(regSubKey, regValue);
	}
	catch (std::exception& e)
	{
		std::cerr << e.what();
	}

	/*FString LibraryPath = FString(valueFromRegistry.c_str()) + "\\Bin\\UE4Export.dll";

	wchar_t* dir = TCHAR_TO_WCHAR(*LibraryPath);

	hModule = ::LoadLibrary(dir);

	if (hModule == NULL)
	{
		LibraryPath = "C:\\Carrot\\Bin\\UE4Export.dll";

		dir = TCHAR_TO_WCHAR(*LibraryPath);

		hModule = ::LoadLibrary(dir);
	}

	if (hModule != NULL)
	{
		showMethod = (ShowObjects*)::GetProcAddress((HMODULE)hModule, "ShowObjects");
		packagedMethod = (ProjectPackaged*)::GetProcAddress((HMODULE)hModule, "ProjectPackaged");
	}*/

	appPath = FString(valueFromRegistry.c_str()) + "\\Bin\\UEExportTemplate.exe";
}

std::wstring FCarrotEditorModule::GetStringValueFromHKLM(const std::wstring& regSubKey, const std::wstring& regValue)
{
	size_t bufferSize = 0xFFF;
	std::wstring valueBuf;
	valueBuf.resize(bufferSize);
	auto cbData = static_cast<DWORD>(bufferSize);
	auto rc = RegGetValueW(
		HKEY_CURRENT_USER,
		regSubKey.c_str(),
		regValue.c_str(),
		RRF_RT_REG_SZ,
		nullptr,
		static_cast<void*>(&valueBuf.at(0)),
		&cbData
	);
	while (rc == ERROR_MORE_DATA)
	{
		cbData /= sizeof(wchar_t);
		if (cbData > static_cast<DWORD>(bufferSize))
		{
			bufferSize = static_cast<size_t>(cbData);
		}
		else
		{
			bufferSize *= 2;
			cbData = static_cast<DWORD>(bufferSize);
		}
		valueBuf.resize(bufferSize);
		rc = RegGetValueW(
			HKEY_LOCAL_MACHINE,
			regSubKey.c_str(),
			regValue.c_str(),
			RRF_RT_REG_SZ,
			nullptr,
			static_cast<void*>(&valueBuf.at(0)),
			&cbData
		);
	}
	if (rc == ERROR_SUCCESS)
	{
		valueBuf.resize(static_cast<size_t>(cbData / sizeof(wchar_t)));
		return valueBuf;
	}
	else
	{
		return TEXT("No Reg key");
	}
}

static UK2Node_SwitchString* FindSwitchString(UEdGraphPin* pin)
{
	if (pin->HasAnyConnections())
	{
		TArray<UEdGraphPin*> connectedPins = pin->LinkedTo;

		for (int i = 0; i < connectedPins.Num(); i++)
		{
			UEdGraphNode* node = connectedPins[i]->GetOwningNode();
			UK2Node_SwitchString* switchNode = Cast<UK2Node_SwitchString>(node);

			if (switchNode != NULL)
			{
				return switchNode;
			}
			else
			{
				TArray<UEdGraphPin*> nodePins = node->Pins;

				for (int j = 0; j < nodePins.Num(); j++)
				{
					if (nodePins[j]->Direction == EEdGraphPinDirection::EGPD_Output)
					{
						UK2Node_SwitchString* switchNode2 = FindSwitchString(nodePins[j]);

						if (switchNode2 != NULL)
						{
							return switchNode2;
						}
					}
				}
			}
		}
	}

	return NULL;
}

static FRttStruct* GetRttStruct(UEdGraphPin* pin)
{
	UObject* obj = pin->DefaultObject;

	if (obj != NULL)
	{
		UTextureRenderTarget2D* rtt = Cast<UTextureRenderTarget2D>(obj);

		if (rtt != NULL)
		{
			FRttStruct* result = new FRttStruct();

			result->Name = obj->GetFName().ToString();
			result->Width = rtt->GetSurfaceWidth();
			result->Height = rtt->GetSurfaceHeight();

			result->WidthString = FString::FromInt(result->Width);
			result->HeightString = FString::FromInt(result->Height);

			return result;
		}
	}

	return NULL;
}

void FCarrotEditorModule::GetComponentsFromGraph(UEdGraph* graph)
{
	TArray<UEdGraphNode*> nodes = graph->Nodes;

	//graph->Schema->IsUnreachable

	int nodeCnt = nodes.Num();

	for (int n = 0; n < nodeCnt; n++)
	{
		UEdGraphNode* node = nodes[n];

		UK2Node_CallFunction* funcNode = Cast<UK2Node_CallFunction>(node);

		if (funcNode != NULL)
		{
			FString funcName = node->GetDescriptiveCompiledName();
			UE_LOG(LogCarrotEditor, Warning, TEXT("Func Name: %s"), *funcName);

			//UEdGraphPin* execpin = funcNode->GetExecPin();
			//
			//if (execpin != NULL && execpin->HasAnyConnections())
			//{
			//	UE_LOG(LogCarrotEditor, Warning, TEXT("Connected!"));
			//}
			//else
			//{
			//	UE_LOG(LogCarrotEditor, Warning, TEXT("Not Connected!"));
			//}

			if (funcName == nmCarrotCommandsFunc)
			{
				UEdGraphPin* pinCommands = node->FindPin(nmCommandsPin, EEdGraphPinDirection::EGPD_Output);

				if (pinCommands != NULL)
				{
					UK2Node_SwitchString* switchNode = FindSwitchString(pinCommands);

					if (switchNode != NULL)
					{
						TArray<FName> pinNames = switchNode->PinNames;

						for (int i = 0; i < pinNames.Num(); i++)
						{
							UE_LOG(LogCarrotEditor, Warning, TEXT("Command: %s"), *pinNames[i].ToString());
							commands.Add(pinNames[i].ToString());
						}
					}
				}
			}
			else if (funcName == nmCarrotTrackingDataFunc)
			{
				hasTrackingData = true;
			}
			else if (funcName == nmCarrotReceiverFunc)
			{
				UEdGraphPin* pinInputRTT = node->FindPin(nmTextureRenderTarget2DPin, EEdGraphPinDirection::EGPD_Input);

				if (pinInputRTT != NULL)
				{
					FRttStruct* rttstruct = GetRttStruct(pinInputRTT);

					if (rttstruct != NULL)
					{
						receivers.Add(rttstruct);

						UE_LOG(LogCarrotEditor, Warning, TEXT("pinInputRTT value: %s"), *rttstruct->Name);
						UE_LOG(LogCarrotEditor, Warning, TEXT("pinInputRTT width: %d"), rttstruct->Width);
						UE_LOG(LogCarrotEditor, Warning, TEXT("pinInputRTT height: %d"), rttstruct->Height);
					}
				}

			}
			else if (funcName == nmCarrotSenderFunc)
			{
				UEdGraphPin* pinOutputRTT = node->FindPin(nmTextureRenderTarget2DPin, EEdGraphPinDirection::EGPD_Input);

				if (pinOutputRTT != NULL)
				{
					FRttStruct* rttstruct = GetRttStruct(pinOutputRTT);

					if (rttstruct != NULL)
					{
						senders.Add(rttstruct);

						UE_LOG(LogCarrotEditor, Warning, TEXT("pinOutputRTT value: %s"), *rttstruct->Name);
						UE_LOG(LogCarrotEditor, Warning, TEXT("pinOutputRTT width: %d"), rttstruct->Width);
						UE_LOG(LogCarrotEditor, Warning, TEXT("pinOutputRTT height: %d"), rttstruct->Height);
					}
				}
			}
			else if (funcName == nmCarrotInitFunc)
			{
				UEdGraphPin* widthPin = node->FindPin(nmCarrotInitWidth, EEdGraphPinDirection::EGPD_Input);

				UEdGraphPin* heigthPin = node->FindPin(nmCarrotInitHeight, EEdGraphPinDirection::EGPD_Input);

				if (widthPin != NULL && heigthPin != NULL)
				{
					FRttStruct* rttstruct = new FRttStruct();

					rttstruct->Name = "OutputViewport";
					rttstruct->WidthString = widthPin->DefaultValue;
					rttstruct->HeightString = heigthPin->DefaultValue;

					if (rttstruct != NULL)
					{
						senders.Add(rttstruct);

						UE_LOG(LogCarrotEditor, Warning, TEXT("OutputViewportRTT value: %s"), *rttstruct->Name);
						UE_LOG(LogCarrotEditor, Warning, TEXT("OutputViewportRTT width: %s"), *rttstruct->WidthString);
						UE_LOG(LogCarrotEditor, Warning, TEXT("OutputViewportRTT height: %s"), *rttstruct->HeightString);
					}
				}
			}

			//---

			const UEdGraphNode* OutGraphNode = NULL;

			UEdGraph* funcGraph = funcNode->GetFunctionGraph(OutGraphNode);

			if (funcGraph != NULL && funcGraph != graph)
			{
				GetComponentsFromGraph(funcGraph);
			}
		}

		UK2Node_MacroInstance* macroNode = Cast<UK2Node_MacroInstance>(node);

		if (macroNode != NULL)
		{
			UEdGraph* macroGraph = macroNode->GetMacroGraph();

			FName macroGraphFName = macroGraph->GetFName();
			UE_LOG(LogCarrotEditor, Warning, TEXT("macroGraphFName: %s"), *macroGraphFName.ToString());

			if (macroGraphFName.ToString().Contains(nmCarrotMacro.ToString()))
			//if (macroGraphFName.IsEqual(nmCarrotMacro))
			{
				UEdGraphPin* pinCommand = node->FindPin(nmCommandPin, EEdGraphPinDirection::EGPD_Output);

				if (pinCommand != NULL)
				{
					UK2Node_SwitchString* switchNode = FindSwitchString(pinCommand);

					if (switchNode != NULL)
					{
						TArray<FName> pinNames = switchNode->PinNames;

						for (int i = 0; i < pinNames.Num(); i++)
						{
							UE_LOG(LogCarrotEditor, Warning, TEXT("Command: %s"), *pinNames[i].ToString());
							commands.Add(pinNames[i].ToString());
						}
					}
				}

				UEdGraphPin* pinInputRTT = node->FindPin(nmInputRenderTarget2DPin, EEdGraphPinDirection::EGPD_Input);

				if (pinInputRTT != NULL)
				{
					FRttStruct* rttstruct = GetRttStruct(pinInputRTT);

					if (rttstruct != NULL)
					{
						receivers.Add(rttstruct);

						UE_LOG(LogCarrotEditor, Warning, TEXT("pinInputRTT value: %s"), *rttstruct->Name);
						UE_LOG(LogCarrotEditor, Warning, TEXT("pinInputRTT width: %d"), rttstruct->Width);
						UE_LOG(LogCarrotEditor, Warning, TEXT("pinInputRTT height: %d"), rttstruct->Height);
					}
				}

				UEdGraphPin* pinOutputRTT = node->FindPin(nmOutputRenderTarget2DPin, EEdGraphPinDirection::EGPD_Input);

				if (pinOutputRTT != NULL)
				{
					FRttStruct* rttstruct = GetRttStruct(pinOutputRTT);

					if (rttstruct != NULL)
					{
						senders.Add(rttstruct);

						UE_LOG(LogCarrotEditor, Warning, TEXT("pinOutputRTT value: %s"), *rttstruct->Name);
						UE_LOG(LogCarrotEditor, Warning, TEXT("pinOutputRTT width: %d"), rttstruct->Width);
						UE_LOG(LogCarrotEditor, Warning, TEXT("pinOutputRTT height: %d"), rttstruct->Height);
					}
				}
			}

			//---

			if (macroGraph != NULL && macroGraph != graph)
			{
				GetComponentsFromGraph(macroGraph);
			}
		}
	}
}

const FString leftT = "<";
const FString leftTTrue = "&lt;";

const FString rightT = ">";
const FString rightTTrue = "&gt;";

const FString and = "&";
const FString andTrue = "&amp;";

const FString quot = "\"";
const FString quotTrue = "&quot;";

const FString apos = "\'";
const FString aposTrue = "&apos;";

void FCarrotEditorModule::BuildStructure(FString projectName, FString levelName)
{
	FString structure = "<Structure>";

	structure += "<ProjectName>";
	structure += projectName;
	structure += "</ProjectName>";

	structure += "<LevelName>";
	structure += levelName;
	structure += "</LevelName>";

	structure += "<MediaSources>";

	for (int r = 0; r < receivers.Num(); r++)
	{
		structure += "<Component>";

		structure += "<Type>";
		structure += "MediaSource";
		structure += "</Type>";

		structure += "<Name>";
		structure += receivers[r]->Name.Replace(*leftT, *leftTTrue).Replace(*rightT, *rightTTrue).Replace(*and, *andTrue).Replace(*quot, *quotTrue).Replace(*apos, *aposTrue);
		structure += "</Name>";

		structure += "<Fields>";

		structure += "<Field>";
		structure += "<Name>";
		structure += "Width";
		structure += "</Name>";
		structure += "<Value>";
		structure.AppendInt(receivers[r]->Width);
		structure += "</Value>";
		structure += "</Field>";

		structure += "<Field>";
		structure += "<Name>";
		structure += "Height";
		structure += "</Name>";
		structure += "<Value>";
		structure.AppendInt(receivers[r]->Height);
		structure += "</Value>";
		structure += "</Field>";

		structure += "</Fields>";

		structure += "</Component>";
	}

	structure += "</MediaSources>";

	structure += "<MediaOutputs>";

	for (int s = 0; s < senders.Num(); s++)
	{
		structure += "<Component>";

		structure += "<Type>";
		structure += "MediaOutput";
		structure += "</Type>";

		structure += "<Name>";
		structure += senders[s]->Name.Replace(*leftT, *leftTTrue).Replace(*rightT, *rightTTrue).Replace(*and, *andTrue).Replace(*quot, *quotTrue).Replace(*apos, *aposTrue);
		structure += "</Name>";

		structure += "<Fields>";

		structure += "<Field>";
		structure += "<Name>";
		structure += "Width";
		structure += "</Name>";
		structure += "<Value>";
		//structure.AppendInt(senders[s]->Width);
		structure += senders[s]->WidthString;
		structure += "</Value>";
		structure += "</Field>";

		structure += "<Field>";
		structure += "<Name>";
		structure += "Height";
		structure += "</Name>";
		structure += "<Value>";
		//structure.AppendInt(senders[s]->Height);
		structure += senders[s]->HeightString;
		structure += "</Value>";
		structure += "</Field>";

		structure += "</Fields>";

		structure += "</Component>";
	}

	structure += "</MediaOutputs>";

	structure += "<TrackingReceivers>";

	if (hasTrackingData)
	{
		structure += "<Component>";

		structure += "<Type>";
		structure += "TrackingDataReceiver";
		structure += "</Type>";

		structure += "<Name>";
		structure += "CarrotTrackingData";
		structure += "</Name>";

		//structure += "<Fields>";
		//
		//structure += "<Field>";
		//structure += "<Name>";
		//structure += "MemoryMappedFileName";
		//structure += "</Name>";
		//structure += "<Value>";
		//structure += memoryMappedFileName;
		//structure += "</Value>";
		//structure += "</Field>";
		//
		//structure += "</Fields>";

		structure += "</Component>";
	}

	structure += "</TrackingReceivers>";

	structure += "<Commands>";

	int cmdCnt = commands.Num();

	for (int c = 0; c < cmdCnt; c++)
	{
		FString command = commands[c];

		structure += "<Component>";

		structure += "<Type>";
		structure += "Command";
		structure += "</Type>";

		structure += "<Name>";
		structure += command.Replace(*leftT, *leftTTrue).Replace(*rightT, *rightTTrue).Replace(*and, *andTrue).Replace(*quot, *quotTrue).Replace(*apos, *aposTrue);
		structure += "</Name>";

		structure += "<Fields>";

		structure += "<Field>";
		structure += "<Name>";
		structure += "ExecutedCommand";
		structure += "</Name>";
		structure += "<Value>";
		structure += command.Replace(*leftT, *leftTTrue).Replace(*rightT, *rightTTrue).Replace(*and, *andTrue).Replace(*quot, *quotTrue).Replace(*apos, *aposTrue);
		structure += "</Value>";
		structure += "</Field>";

		structure += "</Fields>";

		structure += "</Component>";
	}

	structure += "</Commands>";

	structure += "</Structure>";

	SendStructureToCarrot(structure);
}

static void UatCallback(FString result, double time)
{
	fCarrotEditorModule->SendPackagedPathToCarrot(result);
}

void FCarrotEditorModule::SendPackagedPathToCarrot(FString result)
{
	if (packagedMethod != NULL)
	{
		wchar_t* pathWchar;

		if (result == "Completed")
		{
			pathWchar = TCHAR_TO_WCHAR(*PackagedProjectPath);
		}
		else
		{
			pathWchar = TCHAR_TO_WCHAR(*result);
		}

		(*packagedMethod)(pathWchar);
	}
}

static FString PackProject(FString destinationFolder)
{


	FString command;

	command += "BuildCookRun -project=\"";

	FString path = FPaths::GetProjectFilePath();

	FString projectPath = IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(*path);

	command += projectPath;

	command += "\" -noP4 -platform=Win64 -clientconfig=Shipping -serverconfig=Shipping -cook -allmaps -build -stage -pak -archive -archivedirectory=\"";

	command += destinationFolder;

	command += "/\"";

	IUATHelperModule::Get().CreateUatTask(command, FText::FromString("Windows"), FText::FromString("Cooking content"), FText::FromString("Cooking"), nullptr, ::UatCallback);

	FString packagedProjectPath = destinationFolder + "/WindowsNoEditor/" + FApp::GetProjectName() + ".exe";

	return packagedProjectPath;
}

static void PathChanged(const wchar_t* path)
{
	FString destinationFolder = path;

	fCarrotEditorModule->PackagedProjectPath = PackProject(destinationFolder);
}

void FCarrotEditorModule::SendStructureToCarrot(FString structureS)
{
	FPlatformProcess::CreateProc(*appPath, *structureS, true, false, false, nullptr, 0, nullptr, nullptr);

	/*if (showMethod != NULL)
	{
		wchar_t* structure = TCHAR_TO_WCHAR(*structureS);

		(*showMethod)(structure, ::PathChanged);
	}*/
}

int FCarrotEditorModule::FindCarrotLevel(UWorld* World)
{
	int lvlCnt = World->GetLevels().Num();

	for (int l = 0; l < lvlCnt; l++)
	{
		ULevelScriptBlueprint* scriptBlue = World->GetLevels()[l]->GetLevelScriptBlueprint();

		TArray<UEdGraph*> UbergraphPages = scriptBlue->UbergraphPages;

		int graphCnt = UbergraphPages.Num();

		for (int g = 0; g < graphCnt; g++)
		{
			UEdGraph* graph = UbergraphPages[g];

			if (HasCarrotComponents(graph))
			{
				return l;
			}
		}
	}

	return -1;
}

bool FCarrotEditorModule::HasCarrotComponents(UEdGraph* graph)
{
	TArray<UEdGraphNode*> nodes = graph->Nodes;

	int nodeCnt = nodes.Num();

	for (int n = 0; n < nodeCnt; n++)
	{
		UEdGraphNode* node = nodes[n];

		UK2Node_MacroInstance* macroNode = Cast<UK2Node_MacroInstance>(node);

		if (macroNode != NULL)
		{
			UEdGraph* macroGraph = macroNode->GetMacroGraph();

			FName macroGraphFName = macroGraph->GetFName();
			UE_LOG(LogCarrotEditor, Warning, TEXT("macroGraphFName: %s"), *macroGraphFName.ToString());

			if (macroGraphFName.ToString().Contains(nmCarrotMacro.ToString()))
			//if (macroGraphFName.IsEqual(nmCarrotMacro))
			{
				if (macroGraph != NULL && macroGraph != graph)
				{
					return true;
				}
			}
		}
	}

	return false;
}

#undef LOCTEXT_NAMESPACE

IMPLEMENT_MODULE(FCarrotEditorModule, CarrotEditor)
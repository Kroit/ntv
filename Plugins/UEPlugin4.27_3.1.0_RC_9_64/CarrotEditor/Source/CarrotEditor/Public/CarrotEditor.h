// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Modules/ModuleManager.h"

#define WIN32_LEAN_AND_MEAN

#if PLATFORM_WINDOWS
#include "Windows/AllowWindowsPlatformTypes.h"
#endif
#include <windows.h>
#if PLATFORM_WINDOWS
#include "Windows/HideWindowsPlatformTypes.h"
#endif

#include <iostream>

DECLARE_LOG_CATEGORY_EXTERN(LogCarrotEditor, Log, All);

class FToolBarBuilder;
class FMenuBuilder;
class UEdGraphPin;

class FRttStruct
{
public:
	FString Name;
	int Width;
	int Height;
	FString WidthString;
	FString HeightString;
};

class FCarrotEditorModule : public IModuleInterface
{
public:

	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;

	/** This function will be bound to Command. */
	void PluginButtonClicked();

	void SendPackagedPathToCarrot(FString result);

	FString PackagedProjectPath;

private:

	void RegisterMenus();

	void InitDLL();

	std::wstring GetStringValueFromHKLM(const std::wstring& regSubKey, const std::wstring& regValue);

	void GetComponentsFromGraph(UEdGraph* graph);

	void BuildStructure(FString projectName, FString levelName);

	void SendStructureToCarrot(FString structureS);

	bool HasCarrotComponents(UEdGraph* graph);

	int FindCarrotLevel(UWorld* World);

private:
	TSharedPtr<class FUICommandList> PluginCommands;

	HINSTANCE hModule = NULL;

	typedef void (WINAPI PathChanged)(const wchar_t*);

	typedef void (WINAPI ShowObjects)(wchar_t*, PathChanged);

	ShowObjects* showMethod = NULL;

	typedef void (WINAPI ProjectPackaged)(wchar_t*);

	ProjectPackaged* packagedMethod = NULL;

	int e3we3 = 5;

	TArray<FString> commands;
	TArray<FRttStruct*> receivers;
	TArray<FRttStruct*> senders;

	bool hasTrackingData;

	FName nmCarrotMacro = "CarrotMacro";
	FString nmCarrotInitFunc = "CallFunc_CarrotInit";
	FString nmCarrotReceiverFunc = "CallFunc_CarrotReceiver";
	FString nmCarrotSenderFunc = "CallFunc_CarrotSender";
	FString nmCarrotTrackingDataFunc = "CallFunc_CarrotTrackingData";
	FString nmCarrotCommandsFunc = "CallFunc_CarrotCommands";
	FString nmApplyCameraSettingsFunc = "CallFunc_ApplyCameraSettings";
	FName nmTextureRenderTarget2DPin = "textureRenderTarget2D";
	FName nmSceneCaptureComponent2DPin = "sceneCaptureComponent2D";
	FName nmInputRenderTarget2DPin = "InputRenderTarget2D";
	FName nmOutputRenderTarget2DPin = "OutputRenderTarget2D";
	FName nmCommandsPin = "Commands";
	FName nmCommandPin = "Command";
	FName nmCarrotInitWidth = "width";
	FName nmCarrotInitHeight = "height";

	FString appPath;

};

static FCarrotEditorModule* fCarrotEditorModule;
// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class USceneCaptureComponent2D;
class UTextureRenderTarget2D;
class UCameraComponent;
struct FVector;
struct FRotator;
class APlayerController;
#ifdef CARROT_CarrotBPLibrary_generated_h
#error "CarrotBPLibrary.generated.h already included, missing '#pragma once' in CarrotBPLibrary.h"
#endif
#define CARROT_CarrotBPLibrary_generated_h

#define HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotBPLibrary_h_105_SPARSE_DATA
#define HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotBPLibrary_h_105_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execCarrotUpdateCapture); \
	DECLARE_FUNCTION(execCarrotGetViewportSize); \
	DECLARE_FUNCTION(execCarrotGetErrorCode); \
	DECLARE_FUNCTION(execCarrotViewportCopy); \
	DECLARE_FUNCTION(execCarrotViewportSender); \
	DECLARE_FUNCTION(execApplyCameraSettings); \
	DECLARE_FUNCTION(execCarrotCommands); \
	DECLARE_FUNCTION(execCarrotTrackingData); \
	DECLARE_FUNCTION(execCarrotInit); \
	DECLARE_FUNCTION(execCarrotReceiver); \
	DECLARE_FUNCTION(execCarrotSender);


#define HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotBPLibrary_h_105_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execCarrotUpdateCapture); \
	DECLARE_FUNCTION(execCarrotGetViewportSize); \
	DECLARE_FUNCTION(execCarrotGetErrorCode); \
	DECLARE_FUNCTION(execCarrotViewportCopy); \
	DECLARE_FUNCTION(execCarrotViewportSender); \
	DECLARE_FUNCTION(execApplyCameraSettings); \
	DECLARE_FUNCTION(execCarrotCommands); \
	DECLARE_FUNCTION(execCarrotTrackingData); \
	DECLARE_FUNCTION(execCarrotInit); \
	DECLARE_FUNCTION(execCarrotReceiver); \
	DECLARE_FUNCTION(execCarrotSender);


#define HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotBPLibrary_h_105_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCarrotBPLibrary(); \
	friend struct Z_Construct_UClass_UCarrotBPLibrary_Statics; \
public: \
	DECLARE_CLASS(UCarrotBPLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Carrot"), NO_API) \
	DECLARE_SERIALIZER(UCarrotBPLibrary)


#define HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotBPLibrary_h_105_INCLASS \
private: \
	static void StaticRegisterNativesUCarrotBPLibrary(); \
	friend struct Z_Construct_UClass_UCarrotBPLibrary_Statics; \
public: \
	DECLARE_CLASS(UCarrotBPLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Carrot"), NO_API) \
	DECLARE_SERIALIZER(UCarrotBPLibrary)


#define HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotBPLibrary_h_105_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCarrotBPLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCarrotBPLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCarrotBPLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCarrotBPLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCarrotBPLibrary(UCarrotBPLibrary&&); \
	NO_API UCarrotBPLibrary(const UCarrotBPLibrary&); \
public:


#define HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotBPLibrary_h_105_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCarrotBPLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCarrotBPLibrary(UCarrotBPLibrary&&); \
	NO_API UCarrotBPLibrary(const UCarrotBPLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCarrotBPLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCarrotBPLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCarrotBPLibrary)


#define HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotBPLibrary_h_105_PRIVATE_PROPERTY_OFFSET
#define HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotBPLibrary_h_102_PROLOG
#define HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotBPLibrary_h_105_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotBPLibrary_h_105_PRIVATE_PROPERTY_OFFSET \
	HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotBPLibrary_h_105_SPARSE_DATA \
	HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotBPLibrary_h_105_RPC_WRAPPERS \
	HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotBPLibrary_h_105_INCLASS \
	HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotBPLibrary_h_105_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotBPLibrary_h_105_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotBPLibrary_h_105_PRIVATE_PROPERTY_OFFSET \
	HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotBPLibrary_h_105_SPARSE_DATA \
	HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotBPLibrary_h_105_RPC_WRAPPERS_NO_PURE_DECLS \
	HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotBPLibrary_h_105_INCLASS_NO_PURE_DECLS \
	HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotBPLibrary_h_105_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class CarrotBPLibrary."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CARROT_API UClass* StaticClass<class UCarrotBPLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotBPLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS

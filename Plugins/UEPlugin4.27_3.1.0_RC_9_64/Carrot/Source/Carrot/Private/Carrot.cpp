// Copyright Epic Games, Inc. All Rights Reserved.

#include "Carrot.h"

#define LOCTEXT_NAMESPACE "FCarrotModule"

void FCarrotModule::StartupModule()
{
}

void FCarrotModule::ShutdownModule()
{
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FCarrotModule, Carrot)
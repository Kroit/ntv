// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Framework/Commands/Commands.h"
#include "CarrotEditorStyle.h"

class FCarrotEditorCommands : public TCommands<FCarrotEditorCommands>
{
public:

	FCarrotEditorCommands()
		: TCommands<FCarrotEditorCommands>(TEXT("CarrotEditor"), NSLOCTEXT("Contexts", "CarrotEditor", "CarrotEditor Plugin"), NAME_None, FCarrotEditorStyle::GetStyleSetName())
	{
	}

	// TCommands<> interface
	virtual void RegisterCommands() override;

public:
	TSharedPtr< FUICommandInfo > PluginAction;
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameEngine.h"
#include "CarrotGameEngine.generated.h"

/**
 * 
 */
UCLASS()
class CARROT_API UCarrotGameEngine : public UGameEngine
{
	GENERATED_BODY()

public:
	virtual void Init(class IEngineLoop* InEngineLoop) override;
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "CarrotViewportClient.h"

#include "slate/SceneViewport.h"

UCarrotViewportClient::UCarrotViewportClient(FVTableHelper& Helper) : Super(Helper)
{

}

UCarrotViewportClient::~UCarrotViewportClient()
{

}

void UCarrotViewportClient::Init(struct FWorldContext& WorldContext, UGameInstance* OwningGameInstance, bool bCreateNewAudioDevice)
{
	// r.CompositionForceRenderTargetLoad
	IConsoleVariable* const ForceLoadCVar = IConsoleManager::Get().FindConsoleVariable(TEXT("r.CompositionForceRenderTargetLoad"));
	if (ForceLoadCVar)
	{
		ForceLoadCVar->Set(int32(1));
	}

	// r.SceneRenderTargetResizeMethodForceOverride
	IConsoleVariable* const RTResizeForceOverrideCVar = IConsoleManager::Get().FindConsoleVariable(TEXT("r.SceneRenderTargetResizeMethodForceOverride"));
	if (RTResizeForceOverrideCVar)
	{
		RTResizeForceOverrideCVar->Set(int32(1));
	}

	// r.SceneRenderTargetResizeMethod
	IConsoleVariable* const RTResizeCVar = IConsoleManager::Get().FindConsoleVariable(TEXT("r.SceneRenderTargetResizeMethod"));
	if (RTResizeCVar)
	{
		RTResizeCVar->Set(int32(2));
	}

	// RHI.MaximumFrameLatency
	IConsoleVariable* const MaximumFrameLatencyCVar = IConsoleManager::Get().FindConsoleVariable(TEXT("RHI.MaximumFrameLatency"));
	if (MaximumFrameLatencyCVar)
	{
		MaximumFrameLatencyCVar->Set(int32(1));
	}

	UGameViewportClient::Init(WorldContext, OwningGameInstance, bCreateNewAudioDevice);
}

void UCarrotViewportClient::Draw(FViewport* InViewport, FCanvas* SceneCanvas)
{
	if (GEngine)
	{
		viewportTex2d = GEngine->GameViewport->GetGameViewport()->GetRenderTargetTexture().GetReference();
	}

	return UGameViewportClient::Draw(InViewport, SceneCanvas);
}

FTexture2DRHIRef UCarrotViewportClient::GetRenderTargetTex2D()
{
	return viewportTex2d;
}

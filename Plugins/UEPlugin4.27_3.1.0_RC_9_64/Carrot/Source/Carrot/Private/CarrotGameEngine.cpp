// Fill out your copyright notice in the Description page of Project Settings.

#include "CarrotGameEngine.h"

void UCarrotGameEngine::Init(class IEngineLoop* InEngineLoop)
{
	GIsDumpingMovie = 1;

	// Initialize base stuff.
	UGameEngine::Init(InEngineLoop);

	GIsDumpingMovie = 0;
}
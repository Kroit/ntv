// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Carrot/Public/CarrotGameEngine.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCarrotGameEngine() {}
// Cross Module References
	CARROT_API UClass* Z_Construct_UClass_UCarrotGameEngine_NoRegister();
	CARROT_API UClass* Z_Construct_UClass_UCarrotGameEngine();
	ENGINE_API UClass* Z_Construct_UClass_UGameEngine();
	UPackage* Z_Construct_UPackage__Script_Carrot();
// End Cross Module References
	void UCarrotGameEngine::StaticRegisterNativesUCarrotGameEngine()
	{
	}
	UClass* Z_Construct_UClass_UCarrotGameEngine_NoRegister()
	{
		return UCarrotGameEngine::StaticClass();
	}
	struct Z_Construct_UClass_UCarrotGameEngine_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCarrotGameEngine_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UGameEngine,
		(UObject* (*)())Z_Construct_UPackage__Script_Carrot,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCarrotGameEngine_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "CarrotGameEngine.h" },
		{ "ModuleRelativePath", "Public/CarrotGameEngine.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCarrotGameEngine_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCarrotGameEngine>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCarrotGameEngine_Statics::ClassParams = {
		&UCarrotGameEngine::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000AEu,
		METADATA_PARAMS(Z_Construct_UClass_UCarrotGameEngine_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCarrotGameEngine_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCarrotGameEngine()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCarrotGameEngine_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCarrotGameEngine, 2868851632);
	template<> CARROT_API UClass* StaticClass<UCarrotGameEngine>()
	{
		return UCarrotGameEngine::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCarrotGameEngine(Z_Construct_UClass_UCarrotGameEngine, &UCarrotGameEngine::StaticClass, TEXT("/Script/Carrot"), TEXT("UCarrotGameEngine"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCarrotGameEngine);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif

// Copyright Carrot Broadcast, LLC. All Rights Reserved.

using UnrealBuildTool;

public class Carrot : ModuleRules
{
	public Carrot(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;
		
		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"Core",
				"CoreUObject",
				"Engine",
				"RHI",
				"RenderCore"
			});
			
		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				"Slate",
				"SlateCore",
				"TimeManagement",
				"D3D12RHI"
			});

		PublicIncludePaths.AddRange(new string[] {});

		PrivateIncludePaths.AddRange(
			new string[] 
			{
				System.IO.Path.Combine(EngineDirectory, "Source/Runtime/D3D12RHI/Private"),
				System.IO.Path.Combine(EngineDirectory, "Source/Runtime/D3D12RHI/Private/Windows")
			});

		DynamicallyLoadedModuleNames.AddRange(new string[]{});

		AddEngineThirdPartyPrivateStaticDependencies(Target, "DX12");
		AddEngineThirdPartyPrivateStaticDependencies(Target, "NVAftermath");
	}
}

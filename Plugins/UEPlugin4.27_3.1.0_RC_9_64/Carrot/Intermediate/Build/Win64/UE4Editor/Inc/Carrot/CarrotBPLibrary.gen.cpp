// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Carrot/Public/CarrotBPLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCarrotBPLibrary() {}
// Cross Module References
	CARROT_API UClass* Z_Construct_UClass_UCarrotBPLibrary_NoRegister();
	CARROT_API UClass* Z_Construct_UClass_UCarrotBPLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_Carrot();
	ENGINE_API UClass* Z_Construct_UClass_UCameraComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneCaptureComponent2D_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_APlayerController_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UTextureRenderTarget2D_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
// End Cross Module References
	DEFINE_FUNCTION(UCarrotBPLibrary::execCarrotUpdateCapture)
	{
		P_GET_OBJECT(USceneCaptureComponent2D,Z_Param_sceneCaptureComponent2D);
		P_FINISH;
		P_NATIVE_BEGIN;
		UCarrotBPLibrary::CarrotUpdateCapture(Z_Param_sceneCaptureComponent2D);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCarrotBPLibrary::execCarrotGetViewportSize)
	{
		P_GET_PROPERTY_REF(FIntProperty,Z_Param_Out_width);
		P_GET_PROPERTY_REF(FIntProperty,Z_Param_Out_height);
		P_FINISH;
		P_NATIVE_BEGIN;
		UCarrotBPLibrary::CarrotGetViewportSize(Z_Param_Out_width,Z_Param_Out_height);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCarrotBPLibrary::execCarrotGetErrorCode)
	{
		P_GET_PROPERTY_REF(FIntProperty,Z_Param_Out_errcode);
		P_FINISH;
		P_NATIVE_BEGIN;
		UCarrotBPLibrary::CarrotGetErrorCode(Z_Param_Out_errcode);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCarrotBPLibrary::execCarrotViewportCopy)
	{
		P_GET_OBJECT(UTextureRenderTarget2D,Z_Param_textureRenderTarget2D);
		P_FINISH;
		P_NATIVE_BEGIN;
		UCarrotBPLibrary::CarrotViewportCopy(Z_Param_textureRenderTarget2D);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCarrotBPLibrary::execCarrotViewportSender)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		UCarrotBPLibrary::CarrotViewportSender();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCarrotBPLibrary::execApplyCameraSettings)
	{
		P_GET_OBJECT(UCameraComponent,Z_Param_SrcCamera);
		P_GET_OBJECT(USceneCaptureComponent2D,Z_Param_DstCaptureComponent);
		P_FINISH;
		P_NATIVE_BEGIN;
		UCarrotBPLibrary::ApplyCameraSettings(Z_Param_SrcCamera,Z_Param_DstCaptureComponent);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCarrotBPLibrary::execCarrotCommands)
	{
		P_GET_TARRAY_REF(FString,Z_Param_Out_Commands);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UCarrotBPLibrary::CarrotCommands(Z_Param_Out_Commands);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCarrotBPLibrary::execCarrotTrackingData)
	{
		P_GET_STRUCT_REF(FVector,Z_Param_Out_Position);
		P_GET_STRUCT_REF(FRotator,Z_Param_Out_Rotation);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_PositionOffset);
		P_GET_STRUCT_REF(FRotator,Z_Param_Out_RotationOffset);
		P_GET_PROPERTY_REF(FFloatProperty,Z_Param_Out_SensorWidth);
		P_GET_PROPERTY_REF(FFloatProperty,Z_Param_Out_SensorHeight);
		P_GET_PROPERTY_REF(FFloatProperty,Z_Param_Out_FocalLength);
		P_GET_PROPERTY_REF(FFloatProperty,Z_Param_Out_Focus);
		P_GET_PROPERTY_REF(FFloatProperty,Z_Param_Out_FrameNumber);
		P_GET_PROPERTY_REF(FFloatProperty,Z_Param_Out_NodalOffset);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UCarrotBPLibrary::CarrotTrackingData(Z_Param_Out_Position,Z_Param_Out_Rotation,Z_Param_Out_PositionOffset,Z_Param_Out_RotationOffset,Z_Param_Out_SensorWidth,Z_Param_Out_SensorHeight,Z_Param_Out_FocalLength,Z_Param_Out_Focus,Z_Param_Out_FrameNumber,Z_Param_Out_NodalOffset);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCarrotBPLibrary::execCarrotInit)
	{
		P_GET_OBJECT(APlayerController,Z_Param_playerController);
		P_GET_PROPERTY(FIntProperty,Z_Param_width);
		P_GET_PROPERTY(FIntProperty,Z_Param_height);
		P_FINISH;
		P_NATIVE_BEGIN;
		UCarrotBPLibrary::CarrotInit(Z_Param_playerController,Z_Param_width,Z_Param_height);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCarrotBPLibrary::execCarrotReceiver)
	{
		P_GET_OBJECT(UTextureRenderTarget2D,Z_Param_textureRenderTarget2D);
		P_FINISH;
		P_NATIVE_BEGIN;
		UCarrotBPLibrary::CarrotReceiver(Z_Param_textureRenderTarget2D);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCarrotBPLibrary::execCarrotSender)
	{
		P_GET_OBJECT(UTextureRenderTarget2D,Z_Param_textureRenderTarget2D);
		P_FINISH;
		P_NATIVE_BEGIN;
		UCarrotBPLibrary::CarrotSender(Z_Param_textureRenderTarget2D);
		P_NATIVE_END;
	}
	void UCarrotBPLibrary::StaticRegisterNativesUCarrotBPLibrary()
	{
		UClass* Class = UCarrotBPLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ApplyCameraSettings", &UCarrotBPLibrary::execApplyCameraSettings },
			{ "CarrotCommands", &UCarrotBPLibrary::execCarrotCommands },
			{ "CarrotGetErrorCode", &UCarrotBPLibrary::execCarrotGetErrorCode },
			{ "CarrotGetViewportSize", &UCarrotBPLibrary::execCarrotGetViewportSize },
			{ "CarrotInit", &UCarrotBPLibrary::execCarrotInit },
			{ "CarrotReceiver", &UCarrotBPLibrary::execCarrotReceiver },
			{ "CarrotSender", &UCarrotBPLibrary::execCarrotSender },
			{ "CarrotTrackingData", &UCarrotBPLibrary::execCarrotTrackingData },
			{ "CarrotUpdateCapture", &UCarrotBPLibrary::execCarrotUpdateCapture },
			{ "CarrotViewportCopy", &UCarrotBPLibrary::execCarrotViewportCopy },
			{ "CarrotViewportSender", &UCarrotBPLibrary::execCarrotViewportSender },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UCarrotBPLibrary_ApplyCameraSettings_Statics
	{
		struct CarrotBPLibrary_eventApplyCameraSettings_Parms
		{
			UCameraComponent* SrcCamera;
			USceneCaptureComponent2D* DstCaptureComponent;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SrcCamera_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SrcCamera;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DstCaptureComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DstCaptureComponent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCarrotBPLibrary_ApplyCameraSettings_Statics::NewProp_SrcCamera_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCarrotBPLibrary_ApplyCameraSettings_Statics::NewProp_SrcCamera = { "SrcCamera", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CarrotBPLibrary_eventApplyCameraSettings_Parms, SrcCamera), Z_Construct_UClass_UCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UCarrotBPLibrary_ApplyCameraSettings_Statics::NewProp_SrcCamera_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCarrotBPLibrary_ApplyCameraSettings_Statics::NewProp_SrcCamera_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCarrotBPLibrary_ApplyCameraSettings_Statics::NewProp_DstCaptureComponent_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCarrotBPLibrary_ApplyCameraSettings_Statics::NewProp_DstCaptureComponent = { "DstCaptureComponent", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CarrotBPLibrary_eventApplyCameraSettings_Parms, DstCaptureComponent), Z_Construct_UClass_USceneCaptureComponent2D_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UCarrotBPLibrary_ApplyCameraSettings_Statics::NewProp_DstCaptureComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCarrotBPLibrary_ApplyCameraSettings_Statics::NewProp_DstCaptureComponent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCarrotBPLibrary_ApplyCameraSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCarrotBPLibrary_ApplyCameraSettings_Statics::NewProp_SrcCamera,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCarrotBPLibrary_ApplyCameraSettings_Statics::NewProp_DstCaptureComponent,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCarrotBPLibrary_ApplyCameraSettings_Statics::Function_MetaDataParams[] = {
		{ "Category", "Carrot" },
		{ "ModuleRelativePath", "Public/CarrotBPLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCarrotBPLibrary_ApplyCameraSettings_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCarrotBPLibrary, nullptr, "ApplyCameraSettings", nullptr, nullptr, sizeof(CarrotBPLibrary_eventApplyCameraSettings_Parms), Z_Construct_UFunction_UCarrotBPLibrary_ApplyCameraSettings_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCarrotBPLibrary_ApplyCameraSettings_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCarrotBPLibrary_ApplyCameraSettings_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCarrotBPLibrary_ApplyCameraSettings_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCarrotBPLibrary_ApplyCameraSettings()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCarrotBPLibrary_ApplyCameraSettings_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCarrotBPLibrary_CarrotCommands_Statics
	{
		struct CarrotBPLibrary_eventCarrotCommands_Parms
		{
			TArray<FString> Commands;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Commands_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Commands;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UCarrotBPLibrary_CarrotCommands_Statics::NewProp_Commands_Inner = { "Commands", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UCarrotBPLibrary_CarrotCommands_Statics::NewProp_Commands = { "Commands", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CarrotBPLibrary_eventCarrotCommands_Parms, Commands), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UCarrotBPLibrary_CarrotCommands_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((CarrotBPLibrary_eventCarrotCommands_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCarrotBPLibrary_CarrotCommands_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(CarrotBPLibrary_eventCarrotCommands_Parms), &Z_Construct_UFunction_UCarrotBPLibrary_CarrotCommands_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCarrotBPLibrary_CarrotCommands_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCarrotBPLibrary_CarrotCommands_Statics::NewProp_Commands_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCarrotBPLibrary_CarrotCommands_Statics::NewProp_Commands,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCarrotBPLibrary_CarrotCommands_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCarrotBPLibrary_CarrotCommands_Statics::Function_MetaDataParams[] = {
		{ "Category", "Carrot" },
		{ "ModuleRelativePath", "Public/CarrotBPLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCarrotBPLibrary_CarrotCommands_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCarrotBPLibrary, nullptr, "CarrotCommands", nullptr, nullptr, sizeof(CarrotBPLibrary_eventCarrotCommands_Parms), Z_Construct_UFunction_UCarrotBPLibrary_CarrotCommands_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCarrotBPLibrary_CarrotCommands_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCarrotBPLibrary_CarrotCommands_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCarrotBPLibrary_CarrotCommands_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCarrotBPLibrary_CarrotCommands()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCarrotBPLibrary_CarrotCommands_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCarrotBPLibrary_CarrotGetErrorCode_Statics
	{
		struct CarrotBPLibrary_eventCarrotGetErrorCode_Parms
		{
			int32 errcode;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_errcode;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UCarrotBPLibrary_CarrotGetErrorCode_Statics::NewProp_errcode = { "errcode", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CarrotBPLibrary_eventCarrotGetErrorCode_Parms, errcode), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCarrotBPLibrary_CarrotGetErrorCode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCarrotBPLibrary_CarrotGetErrorCode_Statics::NewProp_errcode,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCarrotBPLibrary_CarrotGetErrorCode_Statics::Function_MetaDataParams[] = {
		{ "Category", "Carrot" },
		{ "ModuleRelativePath", "Public/CarrotBPLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCarrotBPLibrary_CarrotGetErrorCode_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCarrotBPLibrary, nullptr, "CarrotGetErrorCode", nullptr, nullptr, sizeof(CarrotBPLibrary_eventCarrotGetErrorCode_Parms), Z_Construct_UFunction_UCarrotBPLibrary_CarrotGetErrorCode_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCarrotBPLibrary_CarrotGetErrorCode_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCarrotBPLibrary_CarrotGetErrorCode_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCarrotBPLibrary_CarrotGetErrorCode_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCarrotBPLibrary_CarrotGetErrorCode()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCarrotBPLibrary_CarrotGetErrorCode_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCarrotBPLibrary_CarrotGetViewportSize_Statics
	{
		struct CarrotBPLibrary_eventCarrotGetViewportSize_Parms
		{
			int32 width;
			int32 height;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_width;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_height;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UCarrotBPLibrary_CarrotGetViewportSize_Statics::NewProp_width = { "width", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CarrotBPLibrary_eventCarrotGetViewportSize_Parms, width), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UCarrotBPLibrary_CarrotGetViewportSize_Statics::NewProp_height = { "height", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CarrotBPLibrary_eventCarrotGetViewportSize_Parms, height), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCarrotBPLibrary_CarrotGetViewportSize_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCarrotBPLibrary_CarrotGetViewportSize_Statics::NewProp_width,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCarrotBPLibrary_CarrotGetViewportSize_Statics::NewProp_height,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCarrotBPLibrary_CarrotGetViewportSize_Statics::Function_MetaDataParams[] = {
		{ "Category", "Carrot" },
		{ "ModuleRelativePath", "Public/CarrotBPLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCarrotBPLibrary_CarrotGetViewportSize_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCarrotBPLibrary, nullptr, "CarrotGetViewportSize", nullptr, nullptr, sizeof(CarrotBPLibrary_eventCarrotGetViewportSize_Parms), Z_Construct_UFunction_UCarrotBPLibrary_CarrotGetViewportSize_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCarrotBPLibrary_CarrotGetViewportSize_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCarrotBPLibrary_CarrotGetViewportSize_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCarrotBPLibrary_CarrotGetViewportSize_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCarrotBPLibrary_CarrotGetViewportSize()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCarrotBPLibrary_CarrotGetViewportSize_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCarrotBPLibrary_CarrotInit_Statics
	{
		struct CarrotBPLibrary_eventCarrotInit_Parms
		{
			APlayerController* playerController;
			int32 width;
			int32 height;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_playerController;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_width;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_height;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCarrotBPLibrary_CarrotInit_Statics::NewProp_playerController = { "playerController", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CarrotBPLibrary_eventCarrotInit_Parms, playerController), Z_Construct_UClass_APlayerController_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UCarrotBPLibrary_CarrotInit_Statics::NewProp_width = { "width", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CarrotBPLibrary_eventCarrotInit_Parms, width), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UCarrotBPLibrary_CarrotInit_Statics::NewProp_height = { "height", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CarrotBPLibrary_eventCarrotInit_Parms, height), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCarrotBPLibrary_CarrotInit_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCarrotBPLibrary_CarrotInit_Statics::NewProp_playerController,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCarrotBPLibrary_CarrotInit_Statics::NewProp_width,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCarrotBPLibrary_CarrotInit_Statics::NewProp_height,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCarrotBPLibrary_CarrotInit_Statics::Function_MetaDataParams[] = {
		{ "Category", "Carrot" },
		{ "ModuleRelativePath", "Public/CarrotBPLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCarrotBPLibrary_CarrotInit_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCarrotBPLibrary, nullptr, "CarrotInit", nullptr, nullptr, sizeof(CarrotBPLibrary_eventCarrotInit_Parms), Z_Construct_UFunction_UCarrotBPLibrary_CarrotInit_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCarrotBPLibrary_CarrotInit_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCarrotBPLibrary_CarrotInit_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCarrotBPLibrary_CarrotInit_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCarrotBPLibrary_CarrotInit()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCarrotBPLibrary_CarrotInit_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCarrotBPLibrary_CarrotReceiver_Statics
	{
		struct CarrotBPLibrary_eventCarrotReceiver_Parms
		{
			UTextureRenderTarget2D* textureRenderTarget2D;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_textureRenderTarget2D;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCarrotBPLibrary_CarrotReceiver_Statics::NewProp_textureRenderTarget2D = { "textureRenderTarget2D", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CarrotBPLibrary_eventCarrotReceiver_Parms, textureRenderTarget2D), Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCarrotBPLibrary_CarrotReceiver_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCarrotBPLibrary_CarrotReceiver_Statics::NewProp_textureRenderTarget2D,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCarrotBPLibrary_CarrotReceiver_Statics::Function_MetaDataParams[] = {
		{ "Category", "Carrot" },
		{ "ModuleRelativePath", "Public/CarrotBPLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCarrotBPLibrary_CarrotReceiver_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCarrotBPLibrary, nullptr, "CarrotReceiver", nullptr, nullptr, sizeof(CarrotBPLibrary_eventCarrotReceiver_Parms), Z_Construct_UFunction_UCarrotBPLibrary_CarrotReceiver_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCarrotBPLibrary_CarrotReceiver_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCarrotBPLibrary_CarrotReceiver_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCarrotBPLibrary_CarrotReceiver_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCarrotBPLibrary_CarrotReceiver()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCarrotBPLibrary_CarrotReceiver_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCarrotBPLibrary_CarrotSender_Statics
	{
		struct CarrotBPLibrary_eventCarrotSender_Parms
		{
			UTextureRenderTarget2D* textureRenderTarget2D;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_textureRenderTarget2D;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCarrotBPLibrary_CarrotSender_Statics::NewProp_textureRenderTarget2D = { "textureRenderTarget2D", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CarrotBPLibrary_eventCarrotSender_Parms, textureRenderTarget2D), Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCarrotBPLibrary_CarrotSender_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCarrotBPLibrary_CarrotSender_Statics::NewProp_textureRenderTarget2D,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCarrotBPLibrary_CarrotSender_Statics::Function_MetaDataParams[] = {
		{ "Category", "Carrot" },
		{ "ModuleRelativePath", "Public/CarrotBPLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCarrotBPLibrary_CarrotSender_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCarrotBPLibrary, nullptr, "CarrotSender", nullptr, nullptr, sizeof(CarrotBPLibrary_eventCarrotSender_Parms), Z_Construct_UFunction_UCarrotBPLibrary_CarrotSender_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCarrotBPLibrary_CarrotSender_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCarrotBPLibrary_CarrotSender_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCarrotBPLibrary_CarrotSender_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCarrotBPLibrary_CarrotSender()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCarrotBPLibrary_CarrotSender_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCarrotBPLibrary_CarrotTrackingData_Statics
	{
		struct CarrotBPLibrary_eventCarrotTrackingData_Parms
		{
			FVector Position;
			FRotator Rotation;
			FVector PositionOffset;
			FRotator RotationOffset;
			float SensorWidth;
			float SensorHeight;
			float FocalLength;
			float Focus;
			float FrameNumber;
			float NodalOffset;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Position;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Rotation;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PositionOffset;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RotationOffset;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SensorWidth;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SensorHeight;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FocalLength;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Focus;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FrameNumber;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_NodalOffset;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UCarrotBPLibrary_CarrotTrackingData_Statics::NewProp_Position = { "Position", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CarrotBPLibrary_eventCarrotTrackingData_Parms, Position), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UCarrotBPLibrary_CarrotTrackingData_Statics::NewProp_Rotation = { "Rotation", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CarrotBPLibrary_eventCarrotTrackingData_Parms, Rotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UCarrotBPLibrary_CarrotTrackingData_Statics::NewProp_PositionOffset = { "PositionOffset", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CarrotBPLibrary_eventCarrotTrackingData_Parms, PositionOffset), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UCarrotBPLibrary_CarrotTrackingData_Statics::NewProp_RotationOffset = { "RotationOffset", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CarrotBPLibrary_eventCarrotTrackingData_Parms, RotationOffset), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UCarrotBPLibrary_CarrotTrackingData_Statics::NewProp_SensorWidth = { "SensorWidth", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CarrotBPLibrary_eventCarrotTrackingData_Parms, SensorWidth), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UCarrotBPLibrary_CarrotTrackingData_Statics::NewProp_SensorHeight = { "SensorHeight", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CarrotBPLibrary_eventCarrotTrackingData_Parms, SensorHeight), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UCarrotBPLibrary_CarrotTrackingData_Statics::NewProp_FocalLength = { "FocalLength", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CarrotBPLibrary_eventCarrotTrackingData_Parms, FocalLength), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UCarrotBPLibrary_CarrotTrackingData_Statics::NewProp_Focus = { "Focus", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CarrotBPLibrary_eventCarrotTrackingData_Parms, Focus), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UCarrotBPLibrary_CarrotTrackingData_Statics::NewProp_FrameNumber = { "FrameNumber", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CarrotBPLibrary_eventCarrotTrackingData_Parms, FrameNumber), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UCarrotBPLibrary_CarrotTrackingData_Statics::NewProp_NodalOffset = { "NodalOffset", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CarrotBPLibrary_eventCarrotTrackingData_Parms, NodalOffset), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UCarrotBPLibrary_CarrotTrackingData_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((CarrotBPLibrary_eventCarrotTrackingData_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCarrotBPLibrary_CarrotTrackingData_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(CarrotBPLibrary_eventCarrotTrackingData_Parms), &Z_Construct_UFunction_UCarrotBPLibrary_CarrotTrackingData_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCarrotBPLibrary_CarrotTrackingData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCarrotBPLibrary_CarrotTrackingData_Statics::NewProp_Position,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCarrotBPLibrary_CarrotTrackingData_Statics::NewProp_Rotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCarrotBPLibrary_CarrotTrackingData_Statics::NewProp_PositionOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCarrotBPLibrary_CarrotTrackingData_Statics::NewProp_RotationOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCarrotBPLibrary_CarrotTrackingData_Statics::NewProp_SensorWidth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCarrotBPLibrary_CarrotTrackingData_Statics::NewProp_SensorHeight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCarrotBPLibrary_CarrotTrackingData_Statics::NewProp_FocalLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCarrotBPLibrary_CarrotTrackingData_Statics::NewProp_Focus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCarrotBPLibrary_CarrotTrackingData_Statics::NewProp_FrameNumber,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCarrotBPLibrary_CarrotTrackingData_Statics::NewProp_NodalOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCarrotBPLibrary_CarrotTrackingData_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCarrotBPLibrary_CarrotTrackingData_Statics::Function_MetaDataParams[] = {
		{ "Category", "Carrot" },
		{ "ModuleRelativePath", "Public/CarrotBPLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCarrotBPLibrary_CarrotTrackingData_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCarrotBPLibrary, nullptr, "CarrotTrackingData", nullptr, nullptr, sizeof(CarrotBPLibrary_eventCarrotTrackingData_Parms), Z_Construct_UFunction_UCarrotBPLibrary_CarrotTrackingData_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCarrotBPLibrary_CarrotTrackingData_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C22401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCarrotBPLibrary_CarrotTrackingData_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCarrotBPLibrary_CarrotTrackingData_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCarrotBPLibrary_CarrotTrackingData()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCarrotBPLibrary_CarrotTrackingData_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCarrotBPLibrary_CarrotUpdateCapture_Statics
	{
		struct CarrotBPLibrary_eventCarrotUpdateCapture_Parms
		{
			USceneCaptureComponent2D* sceneCaptureComponent2D;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_sceneCaptureComponent2D_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_sceneCaptureComponent2D;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCarrotBPLibrary_CarrotUpdateCapture_Statics::NewProp_sceneCaptureComponent2D_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCarrotBPLibrary_CarrotUpdateCapture_Statics::NewProp_sceneCaptureComponent2D = { "sceneCaptureComponent2D", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CarrotBPLibrary_eventCarrotUpdateCapture_Parms, sceneCaptureComponent2D), Z_Construct_UClass_USceneCaptureComponent2D_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UCarrotBPLibrary_CarrotUpdateCapture_Statics::NewProp_sceneCaptureComponent2D_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCarrotBPLibrary_CarrotUpdateCapture_Statics::NewProp_sceneCaptureComponent2D_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCarrotBPLibrary_CarrotUpdateCapture_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCarrotBPLibrary_CarrotUpdateCapture_Statics::NewProp_sceneCaptureComponent2D,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCarrotBPLibrary_CarrotUpdateCapture_Statics::Function_MetaDataParams[] = {
		{ "Category", "Carrot" },
		{ "ModuleRelativePath", "Public/CarrotBPLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCarrotBPLibrary_CarrotUpdateCapture_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCarrotBPLibrary, nullptr, "CarrotUpdateCapture", nullptr, nullptr, sizeof(CarrotBPLibrary_eventCarrotUpdateCapture_Parms), Z_Construct_UFunction_UCarrotBPLibrary_CarrotUpdateCapture_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCarrotBPLibrary_CarrotUpdateCapture_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCarrotBPLibrary_CarrotUpdateCapture_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCarrotBPLibrary_CarrotUpdateCapture_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCarrotBPLibrary_CarrotUpdateCapture()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCarrotBPLibrary_CarrotUpdateCapture_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCarrotBPLibrary_CarrotViewportCopy_Statics
	{
		struct CarrotBPLibrary_eventCarrotViewportCopy_Parms
		{
			UTextureRenderTarget2D* textureRenderTarget2D;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_textureRenderTarget2D;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCarrotBPLibrary_CarrotViewportCopy_Statics::NewProp_textureRenderTarget2D = { "textureRenderTarget2D", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CarrotBPLibrary_eventCarrotViewportCopy_Parms, textureRenderTarget2D), Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCarrotBPLibrary_CarrotViewportCopy_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCarrotBPLibrary_CarrotViewportCopy_Statics::NewProp_textureRenderTarget2D,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCarrotBPLibrary_CarrotViewportCopy_Statics::Function_MetaDataParams[] = {
		{ "Category", "Carrot" },
		{ "ModuleRelativePath", "Public/CarrotBPLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCarrotBPLibrary_CarrotViewportCopy_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCarrotBPLibrary, nullptr, "CarrotViewportCopy", nullptr, nullptr, sizeof(CarrotBPLibrary_eventCarrotViewportCopy_Parms), Z_Construct_UFunction_UCarrotBPLibrary_CarrotViewportCopy_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCarrotBPLibrary_CarrotViewportCopy_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCarrotBPLibrary_CarrotViewportCopy_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCarrotBPLibrary_CarrotViewportCopy_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCarrotBPLibrary_CarrotViewportCopy()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCarrotBPLibrary_CarrotViewportCopy_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCarrotBPLibrary_CarrotViewportSender_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCarrotBPLibrary_CarrotViewportSender_Statics::Function_MetaDataParams[] = {
		{ "Category", "Carrot" },
		{ "ModuleRelativePath", "Public/CarrotBPLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCarrotBPLibrary_CarrotViewportSender_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCarrotBPLibrary, nullptr, "CarrotViewportSender", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCarrotBPLibrary_CarrotViewportSender_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCarrotBPLibrary_CarrotViewportSender_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCarrotBPLibrary_CarrotViewportSender()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCarrotBPLibrary_CarrotViewportSender_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UCarrotBPLibrary_NoRegister()
	{
		return UCarrotBPLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UCarrotBPLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCarrotBPLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_Carrot,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCarrotBPLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCarrotBPLibrary_ApplyCameraSettings, "ApplyCameraSettings" }, // 2535519519
		{ &Z_Construct_UFunction_UCarrotBPLibrary_CarrotCommands, "CarrotCommands" }, // 2039472669
		{ &Z_Construct_UFunction_UCarrotBPLibrary_CarrotGetErrorCode, "CarrotGetErrorCode" }, // 947232796
		{ &Z_Construct_UFunction_UCarrotBPLibrary_CarrotGetViewportSize, "CarrotGetViewportSize" }, // 2224100122
		{ &Z_Construct_UFunction_UCarrotBPLibrary_CarrotInit, "CarrotInit" }, // 1853007736
		{ &Z_Construct_UFunction_UCarrotBPLibrary_CarrotReceiver, "CarrotReceiver" }, // 4032594765
		{ &Z_Construct_UFunction_UCarrotBPLibrary_CarrotSender, "CarrotSender" }, // 2687202034
		{ &Z_Construct_UFunction_UCarrotBPLibrary_CarrotTrackingData, "CarrotTrackingData" }, // 258999966
		{ &Z_Construct_UFunction_UCarrotBPLibrary_CarrotUpdateCapture, "CarrotUpdateCapture" }, // 1684902113
		{ &Z_Construct_UFunction_UCarrotBPLibrary_CarrotViewportCopy, "CarrotViewportCopy" }, // 3530670240
		{ &Z_Construct_UFunction_UCarrotBPLibrary_CarrotViewportSender, "CarrotViewportSender" }, // 2456580383
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCarrotBPLibrary_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "CarrotBPLibrary.h" },
		{ "ModuleRelativePath", "Public/CarrotBPLibrary.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCarrotBPLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCarrotBPLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCarrotBPLibrary_Statics::ClassParams = {
		&UCarrotBPLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCarrotBPLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCarrotBPLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCarrotBPLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCarrotBPLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCarrotBPLibrary, 1357030257);
	template<> CARROT_API UClass* StaticClass<UCarrotBPLibrary>()
	{
		return UCarrotBPLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCarrotBPLibrary(Z_Construct_UClass_UCarrotBPLibrary, &UCarrotBPLibrary::StaticClass, TEXT("/Script/Carrot"), TEXT("UCarrotBPLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCarrotBPLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif

// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CARROT_CarrotViewportClient_generated_h
#error "CarrotViewportClient.generated.h already included, missing '#pragma once' in CarrotViewportClient.h"
#endif
#define CARROT_CarrotViewportClient_generated_h

#define HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotViewportClient_h_18_SPARSE_DATA
#define HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotViewportClient_h_18_RPC_WRAPPERS
#define HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotViewportClient_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotViewportClient_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCarrotViewportClient(); \
	friend struct Z_Construct_UClass_UCarrotViewportClient_Statics; \
public: \
	DECLARE_CLASS(UCarrotViewportClient, UGameViewportClient, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Carrot"), NO_API) \
	DECLARE_SERIALIZER(UCarrotViewportClient)


#define HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotViewportClient_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUCarrotViewportClient(); \
	friend struct Z_Construct_UClass_UCarrotViewportClient_Statics; \
public: \
	DECLARE_CLASS(UCarrotViewportClient, UGameViewportClient, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Carrot"), NO_API) \
	DECLARE_SERIALIZER(UCarrotViewportClient)


#define HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotViewportClient_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCarrotViewportClient(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCarrotViewportClient) \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCarrotViewportClient); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCarrotViewportClient(UCarrotViewportClient&&); \
	NO_API UCarrotViewportClient(const UCarrotViewportClient&); \
public:


#define HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotViewportClient_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCarrotViewportClient(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCarrotViewportClient(UCarrotViewportClient&&); \
	NO_API UCarrotViewportClient(const UCarrotViewportClient&); \
public: \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCarrotViewportClient); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCarrotViewportClient)


#define HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotViewportClient_h_18_PRIVATE_PROPERTY_OFFSET
#define HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotViewportClient_h_15_PROLOG
#define HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotViewportClient_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotViewportClient_h_18_PRIVATE_PROPERTY_OFFSET \
	HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotViewportClient_h_18_SPARSE_DATA \
	HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotViewportClient_h_18_RPC_WRAPPERS \
	HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotViewportClient_h_18_INCLASS \
	HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotViewportClient_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotViewportClient_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotViewportClient_h_18_PRIVATE_PROPERTY_OFFSET \
	HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotViewportClient_h_18_SPARSE_DATA \
	HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotViewportClient_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotViewportClient_h_18_INCLASS_NO_PURE_DECLS \
	HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotViewportClient_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CARROT_API UClass* StaticClass<class UCarrotViewportClient>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID HostProject_Plugins_Carrot_Source_Carrot_Public_CarrotViewportClient_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS

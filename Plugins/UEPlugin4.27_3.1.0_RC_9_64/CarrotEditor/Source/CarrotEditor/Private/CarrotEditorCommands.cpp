// Copyright Epic Games, Inc. All Rights Reserved.

#include "CarrotEditorCommands.h"

#define LOCTEXT_NAMESPACE "FCarrotEditorModule"

void FCarrotEditorCommands::RegisterCommands()
{
	UI_COMMAND(PluginAction, "Export Carrot", "Export As Carrot Template", EUserInterfaceActionType::Button, FInputGesture());
}

#undef LOCTEXT_NAMESPACE

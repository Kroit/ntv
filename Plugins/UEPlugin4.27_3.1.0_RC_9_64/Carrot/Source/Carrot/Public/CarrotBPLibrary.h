// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreUObject.h"
#include "Engine.h"

#define WIN32_LEAN_AND_MEAN

#if PLATFORM_WINDOWS
#include "Windows/AllowWindowsPlatformTypes.h"
#endif

#pragma warning(disable: 4005)

//#include <d3d11.h>
#include <d3d11on12.h>

#if PLATFORM_WINDOWS
#include "Windows/HideWindowsPlatformTypes.h"
#endif
#undef UpdateResource
#undef GetCurrentTime

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "CarrotBPLibrary.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogCarrotPlugin, Log, All);

struct FCarrotTrackingDataInternal
{
	int cameraID;
	int packetNum;
	float X;
	float Y;
	float Z;
	float Pan;
	float Tilt;
	float Roll;
	float normalizedFocus;
	float normalizedZoom;
	float CenterShiftX;
	float CenterShiftY;
	float SensorWidth;
	float SensorHeight;
	float FovX;
	float FovY;
	float FocalLength;
	float AspectRatio;
	float NodalOffset;
	float K1;
	float K2;
	float K3;
	float FrameNumber;
	float Oversize;
	float Depth;
	float xOffset;
	float yOffset;
	float zOffset;
	float panOffset;
	float tiltOffset;
	float rollOffset;
};

struct FReceiverStruct
{
	FName sName;
	ID3D11Texture2D* texReceiver;
	IDXGIKeyedMutex* g_pDXGIKeyedMutex_Receiver;
	HANDLE m_hMapInputHandle;
	unsigned long* m_pBufferInputHandle;
	unsigned long CurrentInputHnd;

	bool IsInitialized;
	bool IsMapped;

	void SetName(FName NewsName)
	{
		sName = NewsName;
	}
};

struct FSenderStruct
{
	FName sName;
	ID3D11Texture2D* texSender;
	IDXGIKeyedMutex* g_pDXGIKeyedMutex_Sender;
	HANDLE m_hMapOutputHandle;
	unsigned long* m_pBufferOutputHandle;
	unsigned long CurrentOutputHnd;

	bool IsInitialized;
	bool IsMapped;

	void SetName(FName NewsName)
	{
		sName = NewsName;
	}
};

UCLASS()
class UCarrotBPLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_UCLASS_BODY()

public:

	UFUNCTION(BlueprintCallable, Category = "Carrot")
		static void CarrotSender(UTextureRenderTarget2D* textureRenderTarget2D);

	UFUNCTION(BlueprintCallable, Category = "Carrot")
		static void CarrotReceiver(UTextureRenderTarget2D* textureRenderTarget2D);

	UFUNCTION(BlueprintCallable, Category = "Carrot")
		static void CarrotInit(APlayerController* playerController, int32 width, int32 height);

	UFUNCTION(BlueprintCallable, Category = "Carrot")
		static bool CarrotTrackingData(
			FVector& Position,
			FRotator& Rotation,
			FVector& PositionOffset,
			FRotator& RotationOffset,
			float& SensorWidth,
			float& SensorHeight,
			float& FocalLength,
			float& Focus,
			float& FrameNumber,
			float& NodalOffset);

	UFUNCTION(BlueprintCallable, Category = "Carrot")
		static bool CarrotCommands(TArray<FString>& Commands);

	UFUNCTION(BlueprintCallable, Category = "Carrot")
		static void ApplyCameraSettings(UCameraComponent* SrcCamera, USceneCaptureComponent2D* DstCaptureComponent);

	UFUNCTION(BlueprintCallable, Category = "Carrot")
		static void CarrotViewportSender();

	UFUNCTION(BlueprintCallable, Category = "Carrot")
		static void CarrotViewportCopy(UTextureRenderTarget2D* textureRenderTarget2D);

	UFUNCTION(BlueprintCallable, Category = "Carrot")
		static void CarrotGetErrorCode(int32& errcode);

	UFUNCTION(BlueprintCallable, Category = "Carrot")
		static void CarrotGetViewportSize(int32& width, int32& height);
	
	UFUNCTION(BlueprintCallable, Category = "Carrot")
		static void CarrotUpdateCapture(USceneCaptureComponent2D* sceneCaptureComponent2D);
};

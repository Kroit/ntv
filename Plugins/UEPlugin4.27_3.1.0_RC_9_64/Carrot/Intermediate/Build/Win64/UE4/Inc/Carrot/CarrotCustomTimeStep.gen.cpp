// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Carrot/Public/CarrotCustomTimeStep.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCarrotCustomTimeStep() {}
// Cross Module References
	CARROT_API UClass* Z_Construct_UClass_UCarrotCustomTimeStep_NoRegister();
	CARROT_API UClass* Z_Construct_UClass_UCarrotCustomTimeStep();
	TIMEMANAGEMENT_API UClass* Z_Construct_UClass_UFixedFrameRateCustomTimeStep();
	UPackage* Z_Construct_UPackage__Script_Carrot();
// End Cross Module References
	void UCarrotCustomTimeStep::StaticRegisterNativesUCarrotCustomTimeStep()
	{
	}
	UClass* Z_Construct_UClass_UCarrotCustomTimeStep_NoRegister()
	{
		return UCarrotCustomTimeStep::StaticClass();
	}
	struct Z_Construct_UClass_UCarrotCustomTimeStep_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCarrotCustomTimeStep_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFixedFrameRateCustomTimeStep,
		(UObject* (*)())Z_Construct_UPackage__Script_Carrot,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCarrotCustomTimeStep_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "CarrotCustomTimeStep.h" },
		{ "ModuleRelativePath", "Public/CarrotCustomTimeStep.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCarrotCustomTimeStep_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCarrotCustomTimeStep>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCarrotCustomTimeStep_Statics::ClassParams = {
		&UCarrotCustomTimeStep::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCarrotCustomTimeStep_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCarrotCustomTimeStep_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCarrotCustomTimeStep()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCarrotCustomTimeStep_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCarrotCustomTimeStep, 3952688941);
	template<> CARROT_API UClass* StaticClass<UCarrotCustomTimeStep>()
	{
		return UCarrotCustomTimeStep::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCarrotCustomTimeStep(Z_Construct_UClass_UCarrotCustomTimeStep, &UCarrotCustomTimeStep::StaticClass, TEXT("/Script/Carrot"), TEXT("UCarrotCustomTimeStep"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCarrotCustomTimeStep);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif

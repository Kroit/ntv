// Fill out your copyright notice in the Description page of Project Settings.


#include "CarrotCustomTimeStep.h"

UCarrotCustomTimeStep::UCarrotCustomTimeStep(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
	, State(ECustomTimeStepSynchronizationState::Closed)
	, bDidAValidUpdateTimeStep(false)
{
	frameRate.Numerator = 50;
	frameRate.Denominator = 1;
}

bool UCarrotCustomTimeStep::Initialize(UEngine* InEngine)
{
	//InEngine->GetFName

	FString evtName = "Carrot.TimeStepEvent.Default";

	carrotEventSync = OpenEventW(0, 0, (wchar_t*)((evtName.GetCharArray().GetData())));

	if (carrotEventSync == nullptr)
	{
		carrotEventSync = CreateEventW(0, false, false, (wchar_t*)((evtName.GetCharArray().GetData())));
	}

	State = ECustomTimeStepSynchronizationState::Synchronizing;

	return true;
}

void UCarrotCustomTimeStep::Shutdown(UEngine* InEngine)
{
}

bool UCarrotCustomTimeStep::UpdateTimeStep(UEngine* InEngine)
{
	if (carrotEventSync != 0)
	{
		const double BeforeTime = FPlatformTime::Seconds();

		WaitForSingleObject(carrotEventSync, 100);

		FApp::SetCurrentTime(FPlatformTime::Seconds());
		FApp::SetDeltaTime(GetFixedFrameRate().AsInterval());

		State = ECustomTimeStepSynchronizationState::Synchronized;
	}
	else
	{
		State = ECustomTimeStepSynchronizationState::Synchronizing;
		return true;
	}

	return false;
}

ECustomTimeStepSynchronizationState UCarrotCustomTimeStep::GetSynchronizationState() const
{
	return State;
}

FFrameRate UCarrotCustomTimeStep::GetFixedFrameRate() const
{
	return frameRate;
}

//~ UObject implementation
//--------------------------------------------------------------------
void UCarrotCustomTimeStep::BeginDestroy()
{
	Super::BeginDestroy();
}
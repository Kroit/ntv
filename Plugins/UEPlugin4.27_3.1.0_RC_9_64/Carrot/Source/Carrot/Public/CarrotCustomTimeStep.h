// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//#if PLATFORM_WINDOWS
//#include "Windows/AllowWindowsPlatformTypes.h"
//#endif
//#include <d3d11.h>
//#if PLATFORM_WINDOWS
//#include "Windows/HideWindowsPlatformTypes.h"
//#endif
#undef UpdateResource
#undef GetCurrentTime

#include "CoreMinimal.h"
#include "FixedFrameRateCustomTimeStep.h"
#include "CarrotCustomTimeStep.generated.h"

/**
 * 
 */
UCLASS()
class CARROT_API UCarrotCustomTimeStep : public UFixedFrameRateCustomTimeStep
{
	GENERATED_UCLASS_BODY()

public:
	//~ UFixedFrameRateCustomTimeStep interface
	virtual bool Initialize(UEngine* InEngine) override;
	virtual void Shutdown(UEngine* InEngine) override;
	virtual bool UpdateTimeStep(UEngine* InEngine) override;
	virtual ECustomTimeStepSynchronizationState GetSynchronizationState() const override;
	virtual FFrameRate GetFixedFrameRate() const override;

	//~ UObject interface
	virtual void BeginDestroy() override;

private:
	FFrameRate frameRate;
	HANDLE carrotEventSync;

	ECustomTimeStepSynchronizationState State;
	bool bDidAValidUpdateTimeStep;
	
};

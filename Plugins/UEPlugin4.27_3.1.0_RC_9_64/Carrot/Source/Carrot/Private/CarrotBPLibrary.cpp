// Copyright Epic Games, Inc. All Rights Reserved.

#include "CarrotBPLibrary.h"
//#include "Components/PlanarReflectionComponent.h"
#include "Carrot.h"
#include "CarrotViewportClient.h"
#include "slate/SceneViewport.h"

//#if PLATFORM_WINDOWS
//#include "D3D12RHIPrivate.h"
//#endif

#pragma comment(lib, "d3d11.lib")

DEFINE_LOG_CATEGORY(LogCarrotPlugin);

ID3D11Device* g_D3D11Device;
ID3D11On12Device* g_D3D1112Device;
ID3D11DeviceContext* g_pImmediateContext = NULL;

static bool senderInitialized = false;
//static bool senderProcessed = false;
static bool trackingInitialized = false;
static bool commandsInitialized = false;
static bool IsDx12 = false;
static bool fpsShown = false;

ID3D11Texture2D* texSender;
IDXGIKeyedMutex* g_pDXGIKeyedMutex_Sender;

HANDLE m_hMapOutHandle = 0;
unsigned long* m_pBufferOutHandle = 0;

HANDLE carrotEventReadTracking = 0;
HANDLE carrotEventWriteTracking = 0;
HANDLE m_hMapTracking = 0;
int* m_pBufferTracking = 0;

HANDLE m_hMapCommands = 0;
int* m_pBufferCommands = 0;

TArray<FReceiverStruct> FReceivers;
TArray<FSenderStruct> FSenders;

UCarrotViewportClient* CarrotViewportClient = nullptr;

FSceneViewport* sceneviewport = nullptr;

int32 error_code = 0;

int32 initial_width = 0;
int32 initial_height = 0;
int32 viewport_width = 0;
int32 viewport_height = 0;

void GetDevice()
{
	IsDx12 = false;

	UE_LOG(LogCarrotPlugin, Warning, TEXT("-----------> Set Default RHI DirectX 11/12"));

	if (GDynamicRHI)
	{
		FString RHIname = GDynamicRHI->GetName();

		if (RHIname == TEXT("D3D11"))
		{
			g_D3D11Device = (ID3D11Device*)GDynamicRHI->RHIGetNativeDevice();
			g_D3D11Device->GetImmediateContext(&g_pImmediateContext);

			UE_LOG(LogCarrotPlugin, Warning, TEXT("-----------> DirectX 11"));
		}
		else if (RHIname == TEXT("D3D12"))
		{
			ID3D12Device* dxDevice12 = static_cast<ID3D12Device*>(GDynamicRHI->RHIGetNativeDevice());
			UINT flags = D3D11_CREATE_DEVICE_BGRA_SUPPORT;
			HRESULT hr = S_OK;

			hr = D3D11On12CreateDevice(
				dxDevice12,
				flags,
				nullptr,
				0,
				nullptr,
				0,
				0,
				&g_D3D11Device,
				&g_pImmediateContext,
				nullptr
			);

			hr = g_D3D11Device->QueryInterface(__uuidof(ID3D11On12Device), (void**)&g_D3D1112Device);

			IsDx12 = true;

			UE_LOG(LogCarrotPlugin, Warning, TEXT("-----------> DirectX 12"));
		}
	}

	if (g_D3D11Device == nullptr)
	{
		UE_LOG(LogCarrotPlugin, Warning, TEXT("-----------> Failed Getting Device!"));
	}
}

bool GetReceiverByName(FName name, FReceiverStruct*& ReceiverStruct) {

	auto MyPredicate = [&](const FReceiverStruct InItem) {return InItem.sName == name; };
	ReceiverStruct = FReceivers.FindByPredicate(MyPredicate);

	bool bIsInListSenders = FReceivers.ContainsByPredicate(MyPredicate);

	if (!bIsInListSenders)
	{
		FReceiverStruct* newFReceiverStruct = new FReceiverStruct();
		newFReceiverStruct->SetName(name);
		newFReceiverStruct->IsInitialized = false;
		newFReceiverStruct->IsMapped = false;
		newFReceiverStruct->CurrentInputHnd = 0;

		newFReceiverStruct->texReceiver = 0;
		newFReceiverStruct->g_pDXGIKeyedMutex_Receiver = 0;
		newFReceiverStruct->m_hMapInputHandle = 0;
		newFReceiverStruct->m_pBufferInputHandle = 0;

		ReceiverStruct = newFReceiverStruct;

		if (!(ReceiverStruct->IsMapped))
		{
			ReceiverStruct->IsMapped = true;

			FString mapname = "Carrot.InputDataMap." + name.ToString();

			ReceiverStruct->m_hMapInputHandle = CreateFileMappingA(INVALID_HANDLE_VALUE,
				NULL,
				PAGE_READWRITE,
				0,
				4096,
				TCHAR_TO_ANSI(*mapname));

			ReceiverStruct->m_pBufferInputHandle = (unsigned long*)MapViewOfFile(ReceiverStruct->m_hMapInputHandle, FILE_MAP_ALL_ACCESS, 0, 0, 4096);

			UE_LOG(LogCarrotPlugin, Warning, TEXT("FileMapping: %s"), *mapname);

			FReceivers.Add(*newFReceiverStruct);
		}

		bIsInListSenders = true;
	}

	return bIsInListSenders;
}

bool GetSenderByName(FName name, FSenderStruct*& SenderStruct) {

	auto MyPredicate = [&](const FSenderStruct InItem) {return InItem.sName == name; };
	SenderStruct = FSenders.FindByPredicate(MyPredicate);

	bool bIsInListSenders = FSenders.ContainsByPredicate(MyPredicate);

	if (!bIsInListSenders)
	{
		FSenderStruct* newFSenderStruct = new FSenderStruct();
		newFSenderStruct->SetName(name);
		newFSenderStruct->IsInitialized = false;
		newFSenderStruct->IsMapped = false;
		newFSenderStruct->CurrentOutputHnd = 0;

		newFSenderStruct->texSender = 0;
		newFSenderStruct->g_pDXGIKeyedMutex_Sender = 0;
		newFSenderStruct->m_hMapOutputHandle = 0;
		newFSenderStruct->m_pBufferOutputHandle = 0;

		SenderStruct = newFSenderStruct;

		if (!(SenderStruct->IsMapped))
		{
			SenderStruct->IsMapped = true;

			FString mapname = "Carrot.OutDataMap." + name.ToString();

			SenderStruct->m_hMapOutputHandle = CreateFileMappingA(INVALID_HANDLE_VALUE,
				NULL,
				PAGE_READWRITE,
				0,
				4096,
				TCHAR_TO_ANSI(*mapname));

			SenderStruct->m_pBufferOutputHandle = (unsigned long*)MapViewOfFile(SenderStruct->m_hMapOutputHandle, FILE_MAP_ALL_ACCESS, 0, 0, 4096);

			UE_LOG(LogCarrotPlugin, Warning, TEXT("FileMapping: %s"), *mapname);

			FSenders.Add(*newFSenderStruct);
		}

		bIsInListSenders = true;
	}

	return bIsInListSenders;
}

UCarrotBPLibrary::UCarrotBPLibrary(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

void UCarrotBPLibrary::CarrotSender(UTextureRenderTarget2D* textureRenderTarget2D)
{
	if (textureRenderTarget2D == nullptr)
	{
		UE_LOG(LogCarrotPlugin, Warning, TEXT("No TextureRenderTarget2D Selected!"));
		return;
	}

	FName texname = textureRenderTarget2D->GetFName();

	FSenderStruct* SenderStruct = 0;

	if (GetSenderByName(texname, SenderStruct))
	{
		ENQUEUE_RENDER_COMMAND(void)(
			[textureRenderTarget2D, SenderStruct](FRHICommandListImmediate& RHICmdList)
			{
				if (g_D3D11Device == nullptr)
				{
					UE_LOG(LogCarrotPlugin, Warning, TEXT("Getting Device..."));
					GetDevice();

					if (g_D3D11Device == nullptr)
					{
						return;
					}
				}

				FTexture2DRHIRef tex2D = textureRenderTarget2D->Resource->TextureRHI->GetTexture2D();

				if (!(SenderStruct->IsInitialized))
				{
					SenderStruct->IsInitialized = true;

					D3D11_TEXTURE2D_DESC desc;
					ZeroMemory(&desc, sizeof(desc));

					if (IsDx12)
					{
						D3D12_RESOURCE_DESC td;
						ID3D12Resource* baseTexture = (ID3D12Resource*)tex2D->GetNativeResource();
						td = baseTexture->GetDesc();

						desc.Width = td.Width;
						desc.Height = td.Height;
						desc.Format = td.Format;
					}
					else
					{
						D3D11_TEXTURE2D_DESC td;
						ID3D11Texture2D* baseTexture = (ID3D11Texture2D*)tex2D->GetNativeResource();
						baseTexture->GetDesc(&td);

						desc.Width = td.Width;
						desc.Height = td.Height;
						desc.Format = td.Format;
					}

					desc.MipLevels = 1;
					desc.ArraySize = 1;
					desc.SampleDesc.Count = 1;
					desc.SampleDesc.Quality = 0;
					desc.Usage = D3D11_USAGE_DEFAULT;
					desc.CPUAccessFlags = 0;
					desc.MiscFlags = D3D11_RESOURCE_MISC_SHARED_KEYEDMUTEX;
					desc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET;

					HRESULT hr = g_D3D11Device->CreateTexture2D(&desc, NULL, &(SenderStruct->texSender));

					IDXGIResource* copy_tex_resource = 0;
					hr = SenderStruct->texSender->QueryInterface(__uuidof(IDXGIResource), (void**)&copy_tex_resource);
					HANDLE copy_tex_shared_handle = 0;
					hr = copy_tex_resource->GetSharedHandle(&copy_tex_shared_handle);

					hr = SenderStruct->texSender->QueryInterface(__uuidof(IDXGIKeyedMutex), (LPVOID*)&(SenderStruct->g_pDXGIKeyedMutex_Sender));

					ULONG hnd2 = HandleToUlong(copy_tex_shared_handle);
					*(SenderStruct->m_pBufferOutputHandle) = hnd2;
				}

				if (IsDx12)
				{
					ID3D11Resource* wrappedRes = nullptr;

					HRESULT hr = S_OK;
					ID3D12Resource* baseTexture = (ID3D12Resource*)tex2D->GetNativeResource();
					D3D11_RESOURCE_FLAGS flags = {};

					hr = g_D3D1112Device->CreateWrappedResource(
						baseTexture,
						&flags,
						D3D12_RESOURCE_STATE_RENDER_TARGET,
						D3D12_RESOURCE_STATE_PRESENT,
						__uuidof(ID3D11Resource),
						(void**)&wrappedRes);

					if (SenderStruct->g_pDXGIKeyedMutex_Sender && SenderStruct->texSender && wrappedRes)
					{
						g_D3D1112Device->AcquireWrappedResources(&wrappedRes, 1);

						SenderStruct->g_pDXGIKeyedMutex_Sender->AcquireSync(0, 100);

						g_pImmediateContext->CopyResource(SenderStruct->texSender, wrappedRes);

						SenderStruct->g_pDXGIKeyedMutex_Sender->ReleaseSync(1);

						g_D3D1112Device->ReleaseWrappedResources(&wrappedRes, 1);
					}

					//g_pImmediateContext->Flush();
				}
				else
				{
					ID3D11Texture2D* baseTexture = (ID3D11Texture2D*)tex2D->GetNativeResource();

					if (SenderStruct->g_pDXGIKeyedMutex_Sender && SenderStruct->texSender && baseTexture)
					{
						SenderStruct->g_pDXGIKeyedMutex_Sender->AcquireSync(0, 100);

						g_pImmediateContext->CopyResource(SenderStruct->texSender, baseTexture);

						SenderStruct->g_pDXGIKeyedMutex_Sender->ReleaseSync(1);
					}

					//g_pImmediateContext->Flush();
				}
			}
		);
	}
}

void UCarrotBPLibrary::CarrotViewportSender()
{
	if (CarrotViewportClient == nullptr)
	{
		UE_LOG(LogCarrotPlugin, Warning, TEXT("No CarrotViewportClient Found!"));
		error_code = 22;
		return;
	}

	if (sceneviewport != nullptr)
	{
		if (viewport_width != initial_width || viewport_height != initial_height)
		{
			sceneviewport->SetViewportSize(initial_width, initial_height);
		}
	}

	ENQUEUE_RENDER_COMMAND(void)(
		[](FRHICommandListImmediate& RHICmdList)
		{
			if (g_D3D11Device == nullptr)
			{
				UE_LOG(LogCarrotPlugin, Warning, TEXT("Getting Device..."));
				GetDevice();

				if (g_D3D11Device == nullptr)
				{
					error_code = 24;
					return;
				}
			}

			if (CarrotViewportClient != nullptr)
			{
				FTexture2DRHIRef tex2D = CarrotViewportClient->GetRenderTargetTex2D();

				if (tex2D == nullptr)
				{
					if (GEngine)
					{
						if (sceneviewport != nullptr)
						{
							auto rttsize = sceneviewport->GetRenderTargetTextureSizeXY();
							viewport_width = rttsize.X;
							viewport_height = rttsize.Y;
						}

						tex2D = GEngine->GameViewport->GetGameViewport()->GetRenderTargetTexture().GetReference();

						if (tex2D == nullptr)
						{
							error_code = 232;

							if (sceneviewport == nullptr)
							{
								error_code = 2321;
							}
							else
							{
								tex2D = sceneviewport->GetRenderTargetTexture().GetReference();

								if (tex2D == nullptr)
								{
									error_code = 2322;

									FViewportRHIRef viewportRHI = sceneviewport->GetViewportRHI();

									if (viewportRHI != nullptr)
									{
										error_code = 23221;

										void* nativeTexture = viewportRHI->GetNativeBackBufferTexture();

										if (nativeTexture != 0)
										{
											error_code = 23222;
										}
									}
								}
								else
								{
									error_code = 0;
								}
							}
						}
						else
						{
							error_code = 0;
						}
					}
					else
					{
						error_code = 231;
					}
				}
				else
				{
					error_code = 0;
				}

				if (error_code == 0)
				{
					if (tex2D == nullptr)
					{
						error_code = 23;
					}
					else
					{
						viewport_width = tex2D->GetSizeX();
						viewport_height = tex2D->GetSizeY();
					}

					if (tex2D != nullptr && initial_width == viewport_width && initial_height == viewport_height)
					{
						if (!senderInitialized)
						{
							senderInitialized = true;

							FString mapname = "Carrot.OutDataMap.Default";

							if (m_hMapOutHandle == 0)
							{
								m_hMapOutHandle = CreateFileMappingA(INVALID_HANDLE_VALUE,
									NULL,
									PAGE_READWRITE,
									0,
									4096,
									TCHAR_TO_ANSI(*mapname));

								m_pBufferOutHandle = (unsigned long*)MapViewOfFile(m_hMapOutHandle, FILE_MAP_ALL_ACCESS, 0, 0, 4096);
							}

							UE_LOG(LogCarrotPlugin, Warning, TEXT("FileMapping: %s"), *mapname);

							D3D11_TEXTURE2D_DESC desc;
							ZeroMemory(&desc, sizeof(desc));

							if (IsDx12)
							{
								D3D12_RESOURCE_DESC td;
								ID3D12Resource* baseTexture = (ID3D12Resource*)tex2D->GetNativeResource();
								td = baseTexture->GetDesc();

								desc.Width = td.Width;
								desc.Height = td.Height;
								desc.Format = td.Format;
							}
							else
							{
								D3D11_TEXTURE2D_DESC td;
								ID3D11Texture2D* baseTexture = (ID3D11Texture2D*)tex2D->GetNativeResource();
								baseTexture->GetDesc(&td);

								desc.Width = td.Width;
								desc.Height = td.Height;
								desc.Format = td.Format;
							}

							desc.MipLevels = 1;
							desc.ArraySize = 1;
							desc.SampleDesc.Count = 1;
							desc.SampleDesc.Quality = 0;
							desc.Usage = D3D11_USAGE_DEFAULT;
							desc.CPUAccessFlags = 0;
							desc.MiscFlags = D3D11_RESOURCE_MISC_SHARED_KEYEDMUTEX;
							desc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET;

							HRESULT hr = g_D3D11Device->CreateTexture2D(&desc, NULL, &texSender);

							IDXGIResource* copy_tex_resource = 0;
							hr = texSender->QueryInterface(__uuidof(IDXGIResource), (void**)&copy_tex_resource);
							HANDLE copy_tex_shared_handle = 0;
							hr = copy_tex_resource->GetSharedHandle(&copy_tex_shared_handle);

							hr = texSender->QueryInterface(__uuidof(IDXGIKeyedMutex), (LPVOID*)&g_pDXGIKeyedMutex_Sender);

							ULONG hnd2 = HandleToUlong(copy_tex_shared_handle);
							*m_pBufferOutHandle = hnd2;
						}

						if (IsDx12)
						{
							ID3D11Resource* wrappedRes = nullptr;

							HRESULT hr = S_OK;
							ID3D12Resource* baseTexture = (ID3D12Resource*)tex2D->GetNativeResource();
							D3D11_RESOURCE_FLAGS flags = {};

							hr = g_D3D1112Device->CreateWrappedResource(
								baseTexture,
								&flags,
								D3D12_RESOURCE_STATE_RENDER_TARGET,
								D3D12_RESOURCE_STATE_PRESENT,
								__uuidof(ID3D11Resource),
								(void**)&wrappedRes);

							if (g_pDXGIKeyedMutex_Sender && texSender && wrappedRes)
							{
								g_D3D1112Device->AcquireWrappedResources(&wrappedRes, 1);

								g_pDXGIKeyedMutex_Sender->AcquireSync(0, 100);

								g_pImmediateContext->CopyResource(texSender, wrappedRes);

								g_pDXGIKeyedMutex_Sender->ReleaseSync(1);

								g_D3D1112Device->ReleaseWrappedResources(&wrappedRes, 1);
							}

							g_pImmediateContext->Flush();

							//senderProcessed = true;
						}
						else
						{
							ID3D11Texture2D* baseTexture = (ID3D11Texture2D*)tex2D->GetNativeResource();

							if (g_pDXGIKeyedMutex_Sender && texSender && baseTexture)
							{
								g_pDXGIKeyedMutex_Sender->AcquireSync(0, 100);

								g_pImmediateContext->CopyResource(texSender, baseTexture);

								g_pDXGIKeyedMutex_Sender->ReleaseSync(1);
							}

							g_pImmediateContext->Flush();

							//senderProcessed = true;
						}
					}
					else
					{
						error_code = 25;
					}
				}
			}
			else
			{
				error_code = 22;
			}
		}
	);
}

void UCarrotBPLibrary::CarrotViewportCopy(UTextureRenderTarget2D* textureRenderTarget2D)
{
	if (textureRenderTarget2D == nullptr)
	{
		UE_LOG(LogCarrotPlugin, Warning, TEXT("No TextureRenderTarget2D Selected!"));
		error_code = 11;
		return;
	}

	if (CarrotViewportClient == nullptr)
	{
		UE_LOG(LogCarrotPlugin, Warning, TEXT("No CarrotViewportClient Found!"));
		error_code = 12;
		return;
	}

	if (sceneviewport != nullptr)
	{
		if (viewport_width != initial_width || viewport_height != initial_height)
		{
			sceneviewport->SetViewportSize(initial_width, initial_height);
		}
	}

	ENQUEUE_RENDER_COMMAND(void)(
		[textureRenderTarget2D](FRHICommandListImmediate& RHICmdList)
		{
			if (g_D3D11Device == nullptr)
			{
				UE_LOG(LogCarrotPlugin, Warning, TEXT("Getting Device..."));
				GetDevice();

				if (g_D3D11Device == nullptr)
				{
					error_code = 14;
					return;
				}
			}

			FTexture2DRHIRef srcRT = CarrotViewportClient->GetRenderTargetTex2D();

			if (srcRT == nullptr)
			{
				if (GEngine)
				{
					if (sceneviewport != nullptr)
					{
						auto rttsize = sceneviewport->GetRenderTargetTextureSizeXY();
						viewport_width = rttsize.X;
						viewport_height = rttsize.Y;
					}
			
					srcRT = GEngine->GameViewport->GetGameViewport()->GetRenderTargetTexture().GetReference();
			
					if (srcRT == nullptr)
					{
						error_code = 132;
			
						if (sceneviewport == nullptr)
						{
							error_code = 1321;
						}
						else
						{
							srcRT = sceneviewport->GetRenderTargetTexture().GetReference();
			
							if (srcRT == nullptr)
							{
								error_code = 1322;
			
								FViewportRHIRef viewportRHI = sceneviewport->GetViewportRHI();
			
								if (viewportRHI != nullptr)
								{
									error_code = 13221;
									
									void* nativeTexture = viewportRHI->GetNativeBackBufferTexture();
			
									if (nativeTexture != 0)
									{
										error_code = 13222;
									}
								}
							}
							else
							{
								error_code = 0;
							}
						}
					}
					else
					{
						error_code = 0;
					}
				}
				else
				{
					error_code = 131;
				}
			}
			else
			{
				error_code = 0;
			}

			if (error_code == 0)
			{
				FTexture2DRHIRef destRT = textureRenderTarget2D->Resource->TextureRHI->GetTexture2D();

				if (destRT == nullptr)
				{
					error_code = 16;
				}

				if (srcRT == nullptr)
				{
					error_code = 13;
				}
				else
				{
					viewport_width = srcRT->GetSizeX();
					viewport_height = srcRT->GetSizeY();
				}

				if (destRT != nullptr && srcRT != nullptr && destRT->GetSizeX() == srcRT->GetSizeX() && destRT->GetSizeY() == srcRT->GetSizeY())
				{
					if (IsDx12)
					{
						ID3D11Resource* destWrappedRes = nullptr;
						ID3D11Resource* srcWrappedRes = nullptr;

						HRESULT hr = S_OK;
						ID3D12Resource* destTexture = (ID3D12Resource*)destRT->GetNativeResource();
						ID3D12Resource* srcTexture = (ID3D12Resource*)srcRT->GetNativeResource();

						D3D11_RESOURCE_FLAGS flags = {};

						hr = g_D3D1112Device->CreateWrappedResource(
							destTexture,
							&flags,
							D3D12_RESOURCE_STATE_RENDER_TARGET,
							D3D12_RESOURCE_STATE_PRESENT,
							__uuidof(ID3D11Resource),
							(void**)&destWrappedRes);

						hr = g_D3D1112Device->CreateWrappedResource(
							srcTexture,
							&flags,
							D3D12_RESOURCE_STATE_RENDER_TARGET,
							D3D12_RESOURCE_STATE_PRESENT,
							__uuidof(ID3D11Resource),
							(void**)&srcWrappedRes);

						if (srcWrappedRes && destWrappedRes)
						{
							g_D3D1112Device->AcquireWrappedResources(&destWrappedRes, 1);
							g_D3D1112Device->AcquireWrappedResources(&srcWrappedRes, 1);

							g_pImmediateContext->CopyResource(destWrappedRes, srcWrappedRes);

							g_D3D1112Device->ReleaseWrappedResources(&srcWrappedRes, 1);
							g_D3D1112Device->ReleaseWrappedResources(&destWrappedRes, 1);
						}

						g_pImmediateContext->Flush();
					}
					else
					{
						ID3D11Texture2D* destTexture = (ID3D11Texture2D*)destRT->GetNativeResource();
						ID3D11Texture2D* srcTexture = (ID3D11Texture2D*)srcRT->GetNativeResource();

						if (srcTexture && destTexture)
						{
							g_pImmediateContext->CopyResource(destTexture, srcTexture);
						}

						g_pImmediateContext->Flush();
					}
				}
				else
				{
					error_code = 15;
				}
			}
		}
	);
}

void UCarrotBPLibrary::CarrotReceiver(UTextureRenderTarget2D* textureRenderTarget2D)
{
	if (textureRenderTarget2D == nullptr)
	{
		UE_LOG(LogCarrotPlugin, Warning, TEXT("No TextureRenderTarget2D Selected!"));
		return;
	}

	FName texname = textureRenderTarget2D->GetFName();
	
	FReceiverStruct* ReceiverStruct = 0;
	
	if (GetReceiverByName(texname, ReceiverStruct))
	{
		ENQUEUE_RENDER_COMMAND(void)
			([textureRenderTarget2D, ReceiverStruct](FRHICommandListImmediate& RHICmdList)
				{
					if (g_D3D11Device == nullptr)
					{
						UE_LOG(LogCarrotPlugin, Warning, TEXT("Getting Device..."));
						GetDevice();

						if (g_D3D11Device == nullptr)
						{
							return;
						}
					}

					unsigned long inputhnd = *(ReceiverStruct->m_pBufferInputHandle);

					if (inputhnd != 0 && ReceiverStruct->CurrentInputHnd != inputhnd)
					{
						ReceiverStruct->IsInitialized = false;
					}

					if (!(ReceiverStruct->IsInitialized) && inputhnd != 0)
					{
						ReceiverStruct->IsInitialized = true;
						ReceiverStruct->CurrentInputHnd = inputhnd;

						HANDLE rec_tex_shared_handle = UlongToHandle(inputhnd);

						IDXGIResource* rec_tex_resource = 0;
						HRESULT hr = g_D3D11Device->OpenSharedResource(rec_tex_shared_handle, __uuidof(ID3D11Texture2D), (void**)&rec_tex_resource);
						hr = rec_tex_resource->QueryInterface(__uuidof(ID3D11Texture2D), (void**)(&(ReceiverStruct->texReceiver)));
						hr = ReceiverStruct->texReceiver->QueryInterface(__uuidof(IDXGIKeyedMutex), (LPVOID*)&(ReceiverStruct->g_pDXGIKeyedMutex_Receiver));
					}

					FTexture2DRHIRef tex2D = textureRenderTarget2D->Resource->TextureRHI->GetTexture2D();

					if (ReceiverStruct->g_pDXGIKeyedMutex_Receiver && ReceiverStruct->texReceiver && tex2D)
					{
						if (IsDx12)
						{
							ID3D11Resource* wrappedRes = nullptr;

							HRESULT hr = S_OK;
							ID3D12Resource* baseTexture = (ID3D12Resource*)tex2D->GetNativeResource();
							D3D11_RESOURCE_FLAGS flags = {};

							hr = g_D3D1112Device->CreateWrappedResource(
								baseTexture,
								&flags,
								D3D12_RESOURCE_STATE_RENDER_TARGET,
								D3D12_RESOURCE_STATE_PRESENT,
								__uuidof(ID3D11Resource),
								(void**)&wrappedRes);

							g_D3D1112Device->AcquireWrappedResources(&wrappedRes, 1);

							ReceiverStruct->g_pDXGIKeyedMutex_Receiver->AcquireSync(1, 100);
							g_pImmediateContext->CopyResource(wrappedRes, ReceiverStruct->texReceiver);
							ReceiverStruct->g_pDXGIKeyedMutex_Receiver->ReleaseSync(0);

							g_D3D1112Device->ReleaseWrappedResources(&wrappedRes, 1);
						}
						else
						{
							ID3D11Texture2D* baseTexture = (ID3D11Texture2D*)tex2D->GetNativeResource();

							ReceiverStruct->g_pDXGIKeyedMutex_Receiver->AcquireSync(1, 100);
							g_pImmediateContext->CopyResource(baseTexture, ReceiverStruct->texReceiver);
							ReceiverStruct->g_pDXGIKeyedMutex_Receiver->ReleaseSync(0);
						}
					}
				}
			);
	}
}

void UCarrotBPLibrary::CarrotInit(APlayerController* playerController, int32 width, int32 height)
{
	initial_width = width;
	initial_height = height;

	for (int i = 0; i < FReceivers.Num(); i++)
	{
		FReceiverStruct* ReceiverStruct = &FReceivers[i];

		if (ReceiverStruct->IsMapped)
		{
			if (ReceiverStruct->m_hMapInputHandle != 0)
			{
				if (ReceiverStruct->m_pBufferInputHandle != 0)
				{
					UnmapViewOfFile(ReceiverStruct->m_pBufferInputHandle);
					ReceiverStruct->m_pBufferInputHandle = 0;
				}

				BOOL res = CloseHandle(ReceiverStruct->m_hMapInputHandle);
				ReceiverStruct->m_hMapInputHandle = 0;

				if (res)
				{
					UE_LOG(LogCarrotPlugin, Warning, TEXT("FileMapping %s closed!"), *ReceiverStruct->sName.ToString());
				}
				else
				{
					UE_LOG(LogCarrotPlugin, Warning, TEXT("FileMapping %s not closed!"), *ReceiverStruct->sName.ToString());
				}
			}
		}
	}

	senderInitialized = false;
	trackingInitialized = false;
	commandsInitialized = false;

	if (m_hMapOutHandle != 0)
	{
		CloseHandle(m_hMapOutHandle);
		m_hMapOutHandle = 0;
		m_pBufferOutHandle = 0;
	}

	FReceivers.Empty();

	if (playerController == nullptr)
	{
		UE_LOG(LogCarrotPlugin, Warning, TEXT("No PlayerController Selected!"));
		return;
	}

	ULocalPlayer* ClientPlayer = playerController->GetLocalPlayer();
	
	if (ClientPlayer == nullptr)
	{
		UE_LOG(LogCarrotPlugin, Warning, TEXT("No LocalPlayer Found!"));
		return;
	}
	
	if (UGameViewportClient* ViewportClient = ClientPlayer->ViewportClient)
	{
		sceneviewport = (FSceneViewport*)ViewportClient->Viewport;
		sceneviewport->SetViewportSize(width, height);

		CarrotViewportClient = (UCarrotViewportClient*)ViewportClient;
	}
}

bool UCarrotBPLibrary::CarrotTrackingData(
	FVector& Position,
	FRotator& Rotation,
	FVector& PositionOffset,
	FRotator& RotationOffset,
	float& SensorWidth,
	float& SensorHeight,
	float& FocalLength,
	float& Focus,
	float& FrameNumber,
	float& NodalOffset)
{
	if (!trackingInitialized)
	{
		trackingInitialized = true;

		FString mmfname = "Carrot.TrackingMap.Default";

		if (m_hMapTracking == 0)
		{
			m_hMapTracking = CreateFileMappingA(INVALID_HANDLE_VALUE,
				NULL,
				PAGE_READWRITE,
				0,
				4096,
				TCHAR_TO_ANSI(*mmfname));

			m_pBufferTracking = (int*)MapViewOfFile(m_hMapTracking, FILE_MAP_ALL_ACCESS, 0, 0, 4096); //one memory page
		}

		UE_LOG(LogCarrotPlugin, Warning, TEXT("FileMapping: %s"), *mmfname);
	}

	int offset = *m_pBufferTracking;

	FCarrotTrackingDataInternal td;

	memcpy(&td, (uint8*)(m_pBufferTracking + offset / 4 + 1), sizeof(FCarrotTrackingDataInternal));

	Position = FVector(-td.Z * 100.0f, td.X * 100.0f, td.Y * 100.0f);
	Rotation = FRotator::MakeFromEuler(FVector(-td.Roll, td.Tilt, -td.Pan));
	PositionOffset = FVector(-td.zOffset * 100.0f, td.xOffset * 100.0f, td.yOffset * 100.0f);
	RotationOffset = FRotator::MakeFromEuler(FVector(td.rollOffset, td.tiltOffset, -td.panOffset));

	if (td.SensorWidth < 0.01f)
	{
		td.SensorWidth = 9.6f;
	}

	if (td.SensorHeight < 0.01f)
	{
		td.SensorHeight = 5.4f;
	}

	SensorWidth = td.SensorWidth;
	SensorHeight = td.SensorHeight;

	if (abs(td.FocalLength) < 0.001f && td.FovX > 0.01f)
	{
		double d = td.SensorWidth / (2.0 * tan(td.FovX * 0.0174533 / 2.0));
		double Fake_fovX = 57.295779513 * 2.0 * atan((td.SensorWidth / 1.0 / 2.0) / d);

		double FovTan = tan(Fake_fovX * 0.0174533 / 2.0);
		td.FocalLength = (td.SensorWidth / 2.0) / FovTan;
	}

	if (td.FocalLength < 0.01f)
	{
		td.FocalLength = 4.2f;
	}

	FocalLength = td.FocalLength;

	Focus = td.Depth;

	FrameNumber = td.FrameNumber;

	NodalOffset = td.NodalOffset;

	return true;
}

bool UCarrotBPLibrary::CarrotCommands(TArray<FString>& Commands)
{
	bool result = false;

	Commands.Empty();

	if (!commandsInitialized)
	{
		commandsInitialized = true;

		FString mmfname = "Carrot.CommandsMap.Default";

		if (m_hMapCommands == 0)
		{
			m_hMapCommands = CreateFileMappingA(INVALID_HANDLE_VALUE,
				NULL,
				PAGE_READWRITE,
				0,
				4096,
				TCHAR_TO_ANSI(*mmfname));

			m_pBufferCommands = (int*)MapViewOfFile(m_hMapCommands, FILE_MAP_ALL_ACCESS, 0, 0, 4096);
		}

		UE_LOG(LogCarrotPlugin, Warning, TEXT("FileMapping: %s"), *mmfname);
	}

	if (m_pBufferCommands != nullptr)
	{
		for (int i = 0; i < 16; ++i)
		{
			int offset = i * 64;
			int numBytes = *(m_pBufferCommands + offset);
			if (numBytes > 0)
			{
				result = true;

				FString cmd = FString(UTF8_TO_TCHAR((uint8*)(m_pBufferCommands + offset / 4 + 1)));
				Commands.Add(cmd.Left(numBytes));
				*(m_pBufferCommands + offset) = 0;
			}
		}
	}

	return result;
}

void UCarrotBPLibrary::ApplyCameraSettings(UCameraComponent* Src, USceneCaptureComponent2D* Dst)
{
	if (Src && Dst)
	{
		Dst->SetWorldLocationAndRotation(Src->GetComponentLocation(), Src->GetComponentRotation());
		Dst->FOVAngle = Src->FieldOfView;

		FMinimalViewInfo CameraViewInfo;
		Src->GetCameraView(/*DeltaTime =*/0.0f, CameraViewInfo);
		
		const FPostProcessSettings& SrcPPSettings = CameraViewInfo.PostProcessSettings;
		FPostProcessSettings& DstPPSettings = Dst->PostProcessSettings;
		
		FWeightedBlendables DstWeightedBlendables = DstPPSettings.WeightedBlendables;

		// Copy all of the post processing settings
		DstPPSettings = SrcPPSettings;

		// But restore the original blendables
		DstPPSettings.WeightedBlendables = DstWeightedBlendables;
	}
}

void UCarrotBPLibrary::CarrotGetErrorCode(int32& errcode)
{
	errcode = error_code;
}

void UCarrotBPLibrary::CarrotGetViewportSize(int32& width, int32& height)
{
	width = viewport_width;
	height = viewport_height;
}

void UCarrotBPLibrary::CarrotUpdateCapture(USceneCaptureComponent2D* sceneCaptureComponent2D)
{
	if (sceneCaptureComponent2D != nullptr)
	{
		sceneCaptureComponent2D->CaptureScene();
	}
}
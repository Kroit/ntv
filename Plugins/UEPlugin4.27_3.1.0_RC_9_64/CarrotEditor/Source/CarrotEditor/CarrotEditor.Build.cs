// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class CarrotEditor : ModuleRules
{
	public CarrotEditor(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;
		
		PublicIncludePaths.AddRange(
			new string[] {
				// ... add public include paths required here ...
			}
			);

		PrivateIncludePathModuleNames.AddRange(
			new string[] {
				"AssetTools",
			}
			);

		PrivateIncludePaths.AddRange(
			new string[] {
			}
			);
			
		
		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"Core",
				"MediaAssets",
				"CoreUObject",
				"TimeManagement",
				"RenderCore",
				"UnrealEd",
				"Carrot",
				"BlueprintGraph"
			}
			);
			
		
		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				"Projects",
				"InputCore",
				"UnrealEd",
				"ToolMenus",
				"CoreUObject",
				"Engine",
				"Slate",
				"SlateCore",
				"Core",
				"TimeManagement",
				"RenderCore",
				"MediaAssets",
				"CinematicCamera",
				"Carrot",
				"BlueprintGraph"
			}
			);
		
		
		DynamicallyLoadedModuleNames.AddRange(
			new string[]
			{
				"Media"
			}
			);
	}
}
